<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<link href="typography.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
	<div id="topContainer">
    	<div id="socialMedia">
        	<div id="twitter">
            	<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Follow Us</div>
                <h3>Twitter</h3>
                <p>Tweet1</p>
                <p>Tweet2</p>
                <p>Tweet3</p>
            </div>
            <div id="facebook">
            	<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Like Us</div>
                <h3>Facebook</h3>
                <p>Wall1</p>
                <p>Wall2</p>
                <p>Wall3</p>
            </div>
    	</div>
    </div>
</div>


<div id="header">
	<div id="headerContainer">
    	<div id="logo">
	        <h2>iamthebestof logo</h2>
        </div>
        <div id="menu">
        	<ul>
        	  <li><a href="#">Home</a></li>
        	  <li><a href="#">Products</a></li>
        	  <li><a href="#">About Us</a></li>
        	  <li><a href="#">Testimonials</a></li>
        	  <li><a href="#">Contact</a></li>
      	  </ul>
       	</div>
    </div>
</div>

<div id="main">
	<div id="mainContainer">
    	video intro
    </div>
</div>

<div id="strap">
	<div id="strapContainer">
    	strap for videos or else
    </div>
</div>

<div id="main">
  <div id="mainContainer">
   	<p><img src="placeholders/iamthebestof-logo.jpg" height="55" width="586"  /></p>
      <h2>Hi my name is Name Here and I run thebestofrichmond</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <h1>Meet the Team</h1>
      	<div id="lineContainer">
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
            <div style="clear:both"></div>
 	  	</div>
      	<div id="lineContainer">
      		<div id="quarterTitle">Name 1</div>
      		<div id="quarterTitle">Name 2</div>
      		<div id="quarterTitle">Name 3</div>
      		<div id="quarterTitle">Name 4</div>
            <div style="clear:both"></div>
    	</div>
    </div>
</div>

<div id="strap-s">
	<div id="strapContainer">
      <h1>Products &amp; Services</h1>
	</div>
</div>



<div id="main">
  <div id="mainContainer">
      	<div id="lineContainer">
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
            <div style="clear:both"></div>
 	  	</div>
      	<div id="lineContainer">
      		<div id="quarterTitle">Name 1</div>
      		<div id="quarterTitle">Name 2</div>
      		<div id="quarterTitle">Name 3</div>
      		<div id="quarterTitle">Name 4</div>
            <div style="clear:both"></div>
    	</div>
      	<div id="lineContainer">
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
            <div style="clear:both"></div>
 	  	</div>
      	<div id="lineContainer">
      		<div id="quarterTitle">Name 1</div>
      		<div id="quarterTitle">Name 2</div>
      		<div id="quarterTitle">Name 3</div>
      		<div id="quarterTitle">Name 4</div>
            <div style="clear:both"></div>
    	</div>
      	<div id="lineContainer">
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
            <div style="clear:both"></div>
 	  	</div>
      	<div id="lineContainer">
      		<div id="quarterTitle">Name 1</div>
      		<div id="quarterTitle">Name 2</div>
      		<div id="quarterTitle">Name 3</div>
      		<div id="quarterTitle">Name 4</div>
            <div style="clear:both"></div>
    	</div>
      	<div id="lineContainer">
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
      		<div id="quarter"></div>
            <div style="clear:both"></div>
 	  	</div>
      	<div id="lineContainer">
      		<div id="quarterTitle">Name 1</div>
      		<div id="quarterTitle">Name 2</div>
      		<div id="quarterTitle">Name 3</div>
      		<div id="quarterTitle">Name 4</div>
            <div style="clear:both"></div>
    	</div>
    </div>
</div>


<div id="strap-s">
	<div id="strapContainer">
      <h1>About Us</h1>
	</div>
</div>

<div id="main">
	<div id="mainContainer">
    	<div id="twoThird">
		  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
		  <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius mod tempor incididunt ut labore et dolore magna aliqua.</p>
		</div>
        <div id="oneThird">
          <p>Increase your online visibility or website traffic</p>
          <p>Promote special offers or event</p>
          <p>Proactive marketing to local people</p>
          <p>Connect with other businesses</p>
          <p>Enhance your credibility</p>
        Increase brand awareness</div>
    <div style="clear:both"></div>
    <p align="center"><img src="images/about-us.jpg" height="305" width="790" style="margin-bottom:-32px;" /></p>
    </div>
</div>



<div id="strap-s">
	<div id="strapContainer">
      <h1>Testimonials</h1>
	</div>
</div>

<div id="main">
	<div id="mainContainer">
		<div id="testimonials">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td valign="top" width="100"><img src="images/quote-left.png" width="82" height="76" alt="''" /></td>
		    	<td valign="middle" style="padding:20px 0;">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incidid unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qu is nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repre henderit in voluptate velit esse. <strong>James Brown – Company Name.</strong><br /></td>
			    <td valign="bottom" width="100" align="right"><img src="images/quote-right.png" width="82" height="76" alt="''" />&nbsp;</td>
			  </tr>
			</table>
        </div>
		<div id="testimonials">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td valign="top" width="100"><img src="images/quote-left.png" width="82" height="76" alt="''" /></td>
		    	<td valign="middle" style="padding:20px 0;">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incidid unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qu is nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repre henderit in voluptate velit esse. <strong>James Brown – Company Name.</strong><br /></td>
			    <td valign="bottom" width="100" align="right"><img src="images/quote-right.png" width="82" height="76" alt="''" />&nbsp;</td>
			  </tr>
			</table>
        </div>
		<div id="testimonials">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td valign="top" width="100"><img src="images/quote-left.png" width="82" height="76" alt="''" /></td>
		    	<td valign="middle" style="padding:20px 0;">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incidid unt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qu is nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repre henderit in voluptate velit esse. <strong>James Brown – Company Name.</strong><br /></td>
			    <td valign="bottom" width="100" align="right"><img src="images/quote-right.png" width="82" height="76" alt="''" />&nbsp;</td>
			  </tr>
			</table>
        </div>
    </div>
</div>


<div id="strap-s">
	<div id="strapContainer">
      <h1>What’s happening at thebestofrichmond</h1>
	</div>
</div>


<div id="main">
	<div id="mainContainer">
		<h1>Local event coming up soon</h1>
		<div id="image"></div>
	        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
		<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius mod tempor incididunt ut labore et dolore magna aliqua. Suspendisse vestibulum dignissim quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<br />
	  </p>
      <div style="clear:both"></div>
      <div id="signup">
		<h1>Sign up</h1>
		<p>If you’d like to come along to the event, please sign up with the below form:</p>
		<p>Name: </p>
	  <p>Email:<br />
	  </p>
		</div>
</div>
</div>




<div id="strap-s">
	<div id="strapContainer">
      <h1>Contact Us</h1>
	</div>
</div>

<div id="main">
	<div id="mainContainer">
    	<div id="leftHalf">
		  <p><img src="placeholders/contactus-ph.gif" width="394" height="381" /></p>
		  <div id="contactUs">
          <p>Address1<br />
            Address2<br />
            City<br />
            Postcode
          </p>
          <p>E: hello@thebestofrichmond.co.uk<br />
            T: 0203 000 0124
          </p>
          <p>&nbsp;</p>
          <p><img src="images/socialmedia.jpg" width="178" height="38" /></p>
          </div>
		</div>
        <div id="rightHalf">
          <p>Increase your online visibility or website traffic</p>
          <p>Promote special offers or event</p>
          <p>Proactive marketing to local people</p>
          <p>Connect with other businesses</p>
          <p>Enhance your credibility</p>
        Increase brand awareness</div>
    	<div style="clear:both"></div>
    	<div id="download">
   	      <p><img src="images/download.png" alt="Download" width="374" height="142" align="right" /><img src="images/think-youre-the-best.png" width="478" height="51" alt="Thin you're the best" /></p>
   	      <p>So you’ve had a look around and want to sign up? No problem just fill in our online application form and we will get in touch.</p>
   	      <p><br />
        Then we get together over a coffee! We like to meet with all potential members so we can find out lots more about you and why your customers recommend you. We write a compelling profile and help you to seek testimonials from your customers. We then work with you to get the most from your membership.</p></div>
    </div>
</div>


</body>
</html>
