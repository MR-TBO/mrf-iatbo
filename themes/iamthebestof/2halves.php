<?php  defined('C5_EXECUTE') or die("Access Denied."); 
$this->inc('elements/header.php'); ?>


<div id="strap-s">
	<div id="strapContainer">
				<?php 
					$as = new Area('Title');
					$as->display($c);
				?>
	</div>
</div>



<div id="main">
  <div id="mainContainer">
				<?php 
					$as = new Area('Main');
					$as->display($c);
				?>
        <div style="clear:both"></div>
  		<div id="leftHalf">
				<?php 
					$as = new Area('Left');
					$as->display($c);
				?>
		</div>
  		<div id="rightHalf">
				<?php 
					$as = new Area('Right');
					$as->display($c);
				?>
		</div>
        <div style="clear:both"></div>
				<?php 
					$as = new Area('Download');
					$as->display($c);
				?>
    </div>
</div>

<div id="strap">
	<div id="strapContainer">
				<?php 
					$as = new Area('Strap');
					$as->display($c);
				?>
    </div>
</div>



<?php  $this->inc('elements/footer.php'); ?>