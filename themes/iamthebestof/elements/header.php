<?php  defined('C5_EXECUTE') or die("Access Denied.");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

<?php  Loader::element('header_required'); ?>

<!-- Site Header Content //-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

<?php
	Loader::model('attribute/categories/collection');
	Loader::model('attribute/categories/user');
	Loader::helper('navigation');	
	$c = Page::getCurrentPage();
	$cPath = $c->getCollectionPath().'/';
	$cHandle = $c->getCollectionHandle();

	$u = new User();
	if ($u->checkLogin() && $cHandle == 'admin') {
		$uID = $u->getUserID();
		$ui = UserInfo::getByID($uID);
		$redirectPage = $ui->getAttribute('login_attribute_redirect_url');
	
		$gU = Group::getByName("iamthebestof"); 
		$gA = Group::getByName("Administrators"); 
	
		if ($redirectPage == $cPath || $u->inGroup($gU) || $u->inGroup($gA) || $u->isSuperUser()) {
//			echo 'you can view this page because it's your own dashboard, or because you are an admin';
		} else {
//			echo 'you cant view this page and should be redirected to your own dashboard';
			header( "Location: /$redirectPage" ) ;
			exit;
		}
	}
?>





</head>

<body>


<div id="top">
	<div id="topContainer">
    	<div id="socialMedia">
        	<div id="twitter">
            	<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Follow Us</div>
				<?php 
					$as = new Area('Twitter');
					$as->display($c);
				?>
            </div>
            <div id="facebook">
            	<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Like Us</div>
				<?php 
					$as = new Area('Facebook');
					$as->display($c);
				?>
            </div>
    	</div>
    </div>
</div>


<div id="header">
	<div id="headerContainer" style=" ">
    	<div id="logo">

				<?php 
					$as = new Area('Logo');    
					$as->display($c);
					 
				?> 
        </div>

    </div>
</div>
           		<?php 
			$u = new User();
			if ($u->isRegistered()) { ?>
				<?php  
				if (Config::get("ENABLE_USER_PROFILES")) {
					$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
				} else {
					$userName = $u->getUserName();
				}
				?>
				<div style="text-align:right; font-size:11px; color:#444; width:900px; margin:0 auto;"><?php echo t('Currently logged in as <b>%s</b>.', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></div>
			<?php  }  ?>
