/*
 * jquery.flexislider.js v0.1 - jQuery script
 * Copyright (c) 2009 Barry Roodt (http://calisza.wordpress.com)
 *
 * Licensed under the New BSD license.
 *
 * This script slides a list of images from right to left across the window.
 * An example can be found at http://flexidev.co.za/projects/flexislider
 * Please check http://code.google.com/p/flexidev/downloads/ for the latest version
 *
 */

	var speed = 40;
	var pic, numImgs, arrLeft, i, totalWidth, n, myInterval; 

jQuery(window).load(function(){
	pic = jQuery("#slider").children("img");
	numImgs = pic.length;
	arrLeft = new Array(numImgs);
	
	for (i=0;i<numImgs;i++){
		
		totalWidth=0;
		for(n=0;n<i;n++){
			totalWidth += jQuery(pic[n]).width();
		}
		
		arrLeft[i] = totalWidth;
		jQuery(pic[i]).css("left",totalWidth);
	}
	
	myInterval = setInterval("flexiScroll()",speed);
	jQuery('#imageloader').hide();
	jQuery(pic).show();	
});

function flexiScroll(){

	for (i=0;i<numImgs;i++){
		arrLeft[i] -= 1;		

		if (arrLeft[i] == -(jQuery(pic[i]).width())){	
			totalWidth = 0;	
			for (n=0;n<numImgs;n++){
				if (n!=i){	
					totalWidth += jQuery(pic[n]).width();
				}			
			}	
			arrLeft[i] =  totalWidth;	
		}					
		jQuery(pic[i]).css("left",arrLeft[i]);
	}
}