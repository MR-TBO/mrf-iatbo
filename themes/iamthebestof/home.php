<?php  defined('C5_EXECUTE') or die("Access Denied."); 
$this->inc('elements/header.php'); ?>



<div id="main">
  <div id="mainContainer">
  		<table width="900" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td colspan="2">
				<?php 
					$as = new Area('Title');
					$as->display($c);
				?>
                </td>
	      </tr>
			  <tr>
			    <td width="600" style="padding-right:20px;" valign="top">
				<?php 
					$as = new Area('Introduction');
					$as->display($c);
				?>
                </td>
			    <td width="300" valign="top">
				<?php 
					$as = new Area('Video');
					$as->display($c);
				?>
                </td>
			  </tr>
		</table>
				<?php 
					$as = new Area('Main');
					$as->display($c);
				?>
<div style="clear:both"></div>
				<?php 
					$as = new Area('Download');
					$as->display($c);
				?>
    </div>
</div>


<div id="strap">
	<div id="strapContainer">
				<?php 
					$as = new Area('Strap');
					$as->display($c);
				?>
    </div>
</div>




<?php  $this->inc('elements/footer.php'); ?>