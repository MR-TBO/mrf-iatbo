<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $this->inc('elements/header.php');
?>

<div id="home-page">
  <div id="main-header" class="<?php echo str_replace(' ', '-', str_replace('?', '', strtolower($c->getCollectionName()))); ?>">
    <div class="content">
      <?php
	$as = new Area('Title');
	$as->display($c);
      ?>
    </div>
    <img src="<?php echo $this->getThemePath(); ?>/images/global/home-header.png" class="header-image" />
  </div>
  <div id="main-content">
    <div id="content-top">
      <div class="introduction">
	<?php
	  $as = new Area('Introduction');
	  $as->display($c);
	?>
      </div>
      <div class="video">
	<?php
	  $as = new Area('Video');
	  $as->display($c);
	?>
      </div>
    </div>
    <div id="content">
      <?php
	$as = new Area('Main');
	$as->display($c);
      ?>
    </div>
  </div>
  <div id="download" class="<?php echo str_replace(' ', '-', str_replace('?', '', strtolower($c->getCollectionName()))); ?>">
    <?php
      $as = new Area('Download');
      $as->display($c);
    ?>
    <img src="<?php echo $this->getThemePath(); ?>/images/global/home-footer.png" class="footer-image" />
  </div>
  <div id="main-footer" class="<?php echo str_replace(' ', '-', str_replace('?', '', strtolower($c->getCollectionName()))); ?>">
    <?php
      $as = new Area('Strap');
      $as->display($c);
    ?>
  </div>
  <div id="subfooter">
    <p> thebestof &copy; <?php echo date("Y"); ?></p>
  </div>
</div>
<?php  $this->inc('elements/footer.php'); ?>