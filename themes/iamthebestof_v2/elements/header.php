<?php defined('C5_EXECUTE') or die("Access Denied."); 
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
	
	<?php 
	
	Loader::model('attribute/categories/collection');
Loader::model('attribute/categories/user');
Loader::helper('navigation');
$c = Page::getCurrentPage();
$cPath = $c->getCollectionPath().'/';
$cHandle = $c->getCollectionHandle();
	
$u = new User();
if ($u->checkLogin() && $cHandle == 'admin') {
	$uID = $u->getUserID();
	$ui = UserInfo::getByID($uID);
	$redirectPage = $ui->getAttribute('login_attribute_redirect_url');

	$gU = Group::getByName("iamthebestof");
	$gA = Group::getByName("Administrators");

	if ($redirectPage == $cPath || $u->inGroup($gU) || $u->inGroup($gA) || $u->isSuperUser()) {
		//			echo 'you can view this page because it's your own dashboard, or because you are an admin';
	} else {
		//			echo 'you cant view this page and should be redirected to your own dashboard';
		header( "Location: /$redirectPage" ) ;
		exit;
	}
}
	
?>
	
	
	
		<link href="<?php echo $this->getThemePath(); ?>/css/reset.css" type="text/css" rel="stylesheet" media="screen, print, projection" />
        <link href="<?php echo $this->getThemePath(); ?>/css/style.css" type="text/css" rel="stylesheet" media="screen, print, projection" />
	
	
	
		<?php  Loader::element('header_required'); ?>
        
<script type="text/javascript" src="//use.typekit.net/iqs5eqd.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		
	</head>
	
	<body>
		<div class="wrapper">
			
			<div id="top">
				<div id="topContainer">
					<div id="socialMedia">
						<div id="twitter">
							<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Follow Us</div>
							<?php
								$as = new Area('Twitter');
								$as->display($c);
							?>
						</div>
						<div id="facebook">
							<div style="background-color:#b6d6f0; padding:5px; color:#FFF; display:inline-block; float:right; font-weight:bold;">Like Us</div>
							<?php
								$as = new Area('Facebook');
								$as->display($c);
							?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div id="header" class="<?php echo str_replace(' ', '-', str_replace('?', '', strtolower($c->getCollectionName()))); ?>">
					
					<?php $logo = $c->getCollectionAttributeValue('local_area'); 
							
					
					?>
					
					<img src="/files/logos/<?php echo str_replace(' ','',strtolower($logo)) ?>.gif" />
					
					<?php
						$as = new Area('Logo');
						$as->display($c);
					?>
				</div>
			</div>
			<?php 
				$u = new User();
				if ($u->isRegistered()) {
					if (Config::get("ENABLE_USER_PROFILES")) {
						$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
					} else {
						$userName = $u->getUserName();
					}
					?>
					<div class="user"><?php echo t('Currently logged in as <b>%s</b>.', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out')?></a></div>
				<?php  }  ?>
