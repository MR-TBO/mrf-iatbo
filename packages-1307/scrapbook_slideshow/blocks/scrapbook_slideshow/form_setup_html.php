<?php  
$scrapbookHelper=Loader::helper('concrete/scrapbook');
$availableScrapbooks = $scrapbookHelper->getAvailableScrapbooks();
?>
<table border="0">
	<tr>
		<td>
			<strong><?php   echo t('Scrapbook')?></strong>
		</td>
		<td>
			<select name="scrapbook">
			<?php  
			foreach($availableScrapbooks as $availableScrapbook){
				echo '<option value="' . $availableScrapbook['arHandle'] . '"';
				if ($availableScrapbook['arHandle'] == $tabObj->scrapbook) {
					echo ' selected="selected" ';
				}
				echo '>' . $availableScrapbook['arHandle'] . '</option>';
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Effect')?></strong>
		</td>
		<td>
			<select name="effect">
				<option value="none" <?php   if ($tabObj->effect == 'none') { echo 'selected="selected"'; } ?>>None</option>
				<option value="blindX" <?php   if ($tabObj->effect == 'blindX') { echo 'selected="selected"'; } ?>>Blind X</option>
				<option value="blindY" <?php   if ($tabObj->effect == 'blindY') { echo 'selected="selected"'; } ?>>Blind Y</option>
				<option value="blindZ" <?php   if ($tabObj->effect == 'blindZ') { echo 'selected="selected"'; } ?>>Blind Z</option>
				<option value="cover" <?php   if ($tabObj->effect == 'cover') { echo 'selected="selected"'; } ?>>Cover</option>
				<option value="curtainX" <?php   if ($tabObj->effect == 'curtainX') { echo 'selected="selected"'; } ?>>Curtain X</option>
				<option value="curtainY" <?php   if ($tabObj->effect == 'curtainY') { echo 'selected="selected"'; } ?>>Curtain Y</option>
				<option value="fade" <?php   if ($tabObj->effect == 'fade') { echo 'selected="selected"'; } ?>>Fade</option>
				<option value="fadeZoom" <?php   if ($tabObj->effect == 'FadeZoom') { echo 'selected="selected"'; } ?>>Fade Zoom</option>
				<option value="growX" <?php   if ($tabObj->effect == 'growX') { echo 'selected="selected"'; } ?>>Grow X</option>
				<option value="growY" <?php   if ($tabObj->effect == 'growY') { echo 'selected="selected"'; } ?>>Grow Y</option>
				<option value="scrollUp" <?php   if ($tabObj->effect == 'scrollUp') { echo 'selected="selected"'; } ?>>Scroll Up</option>
				<option value="scrollDown" <?php   if ($tabObj->effect == 'scrollDown') { echo 'selected="selected"'; } ?>>Scroll Down</option>
				<option value="scrollLeft" <?php   if ($tabObj->effect == 'scrollLeft') { echo 'selected="selected"'; } ?>>Scroll Left</option>
				<option value="scrollRight" <?php   if ($tabObj->effect == 'scrollRight') { echo 'selected="selected"'; } ?>>Scroll Right</option>
				<option value="shuffle" <?php   if ($tabObj->effect == 'shuffle') { echo 'selected="selected"'; } ?>>Shuffle</option>
				<option value="slideX" <?php   if ($tabObj->effect == 'slideX') { echo 'selected="selected"'; } ?>>Slide X</option>
				<option value="slideY" <?php   if ($tabObj->effect == 'slideY') { echo 'selected="selected"'; } ?>>Slide Y</option>
				<option value="toss" <?php   if ($tabObj->effect == 'toss') { echo 'selected="selected"'; } ?>>Toss</option>
				<option value="turnUp" <?php   if ($tabObj->effect == 'Turn Up') { echo 'selected="selected"'; } ?>>Turn Up</option>
				<option value="turnDown" <?php   if ($tabObj->effect == 'turnDown') { echo 'selected="selected"'; } ?>>Turn Down</option>
				<option value="turnLeft" <?php   if ($tabObj->effect == 'turnLeft') { echo 'selected="selected"'; } ?>>Turn Left</option>
				<option value="turnRight" <?php   if ($tabObj->effect == 'turnRight') { echo 'selected="selected"'; } ?>>Turn Right</option>
				<option value="uncover" <?php   if ($tabObj->effect == 'uncover') { echo 'selected="selected"'; } ?>>Uncover</option>
				<option value="wipe" <?php   if ($tabObj->effect == 'wipe') { echo 'selected="selected"'; } ?>>Wipe</option>
				<option value="zoom" <?php   if ($tabObj->effect == 'zoom') { echo 'selected="selected"'; } ?>>Zoom</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php  echo t('Delay')?></strong>
		</td>
		<td>
			<input name="delay" type="text" maxlength="6" size="10" value="<?php   echo $tabObj->delay; ?>" /> ms
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Effect Length')?></strong>
		</td>
		<td>
			<input name="effectLength" type="text" maxlength="6" size="10" value="<?php  echo $tabObj->effectLength; ?>" /> ms
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php  echo t('Disable Auto Play?')?></strong>
		</td>
		<td>
			<input name="autoPlay" type="checkbox" value="1" <?php  echo ($tabObj->autoPlay) ? 'checked="checked"':''?>>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php  echo t('Sync Effect Transitions?')?></strong>
			<div class="ccm-note"><?php  echo t("If your effect isn't working quite right,<br />try checking this box"); ?></div>
		</td>
		<td valign="top">
			<input name="syncEffects" type="checkbox" value="1" <?php  echo ($tabObj->syncEffects) ? 'checked="checked"':''?>>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php  echo t('Pause on Hover?')?></strong>
		</td>
		<td>
			<input name="pauseOnHover" type="checkbox" value="1" <?php  echo ($tabObj->pauseOnHover) ? 'checked="checked"':''?>>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php  echo t('Click Content to Proceed?')?></strong>
		</td>
		<td>
			<input name="clickToProceed" type="checkbox" value="1" <?php  echo ($tabObj->clickToProceed) ? 'checked="checked"':''?>>
		</td>
	</tr>
</table>