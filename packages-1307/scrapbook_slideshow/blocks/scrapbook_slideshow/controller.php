<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

class ScrapbookSlideshowBlockController extends BlockController {
	
	var $pobj;
	
	protected $btTable = 'btScrapbookSlideshow';
	protected $btInterfaceWidth = "400";
	protected $btInterfaceHeight = "280";
	
	public function getBlockTypeDescription() {
		return t("Create a slideshow based on a C5 Scrapbook");
	}
	
	public function getBlockTypeName() {
		return t("Scrapbook Slideshow");
	}
	
	function view(){ 
		$sh=Loader::helper('concrete/scrapbook');
		$html = Loader::helper('html');
	
		$this->set('bID', $this->bID);	
		$this->set('scrapbook', $this->scrapbook);
		$this->set('effect', $this->effect);
		$this->set('delay', $this->delay);
		$this->set('effectLength', $this->effectLength);
		$this->set('syncEffects', $this->syncEffects);
		$this->set('autoPlay', $this->autoPlay);
		$this->set('pauseOnHover', $this->pauseOnHover);
		$this->set('clickToProceed', $this->clickToProceed);		
		
		$sbc = $sh->getGlobalScrapbookPage();
		$cPath=$sbc->getCollectionPath();
		$this->set('scrapbookBlocks', $sbc->getBlocks($this->scrapbook));
	}

	function save($data) {
		$args['scrapbook'] = $data['scrapbook'];
		$args['effect'] = $data['effect'];
		$args['delay'] = intval($data['delay']);
		$args['effectLength'] = intval($data['effectLength']);
		
		if ($data['syncEffects'] == '1') {
			$args['syncEffects'] = '1';
		} else {
			$args['syncEffects'] = '0';
		}
		
		if ($data['autoPlay'] == '1') {
			$args['autoPlay'] = '1';
		} else {
			$args['autoPlay'] = '0';
		}
		
		if ($data['pauseOnHover'] == '1') {
			$args['pauseOnHover'] = '1';
		} else {
			$args['pauseOnHover'] = '0';
		}
		
		if ($data['clickToProceed'] == '1') {
			$args['clickToProceed'] = '1';
		} else {
			$args['clickToProceed'] = '0';
		}
		
		parent::save($args);
	}
	
}

?>