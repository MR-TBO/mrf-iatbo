<?php 
  defined('C5_EXECUTE') or die(_("Access Denied."));

  class JeremeTweetcretePackage extends Package {
    protected $pkgDescription = 'Add a twitter feed to your website.';
    protected $pkgName = "Tweetcrete";
    protected $pkgHandle = 'jereme_tweetcrete';

    protected $appVersionRequired = '5.4.1.1';
    protected $pkgVersion = '1.6.0';

    function install() {
      $pkg = parent::install();
      BlockType::installBlockTypeFromPackage('jereme_tweetcrete', $pkg);
    }

    function upgrade() {
      parent::upgrade();
      /* Refresh all blocks */
      Loader::model('block_types');
      $items = array(BlockType::getByHandle('jereme_tweetcrete'));
      foreach($items as $item) {
        $item->refresh();
      }
    }
  }
?>
