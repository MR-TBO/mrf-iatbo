<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('tweetcrete_tweet','jereme_tweetcrete');
Loader::model('json');

class TweetcreteHashTag {
  private $url = "http://search.twitter.com/search.json?q=%23";
  private $hashtag = "";

  public function getTimeline($hashTag) {
    $this->hashtag = $hashTag;
    
    return $this->toTweetObjects( $this->getRawTimeline() );
  }
  
  private function getRawTimeline() {
    $json = file_get_contents($this->url(),0,null,null);

    $jsonArr = JsonHelper::decode($json, true);

    return $jsonArr->results;
  }
  
  private function toTweetObjects($jsonArr) {
    $tweet_array = array();
    foreach($jsonArr as $slice) {
      $tweet_array[]= TweetcreteTweet::fromJSON($slice);
    }
    
    return $tweet_array;
  }
  
  private function url() {
    return sprintf("%s%s", $this->url, $this->hashtag);
  }
}