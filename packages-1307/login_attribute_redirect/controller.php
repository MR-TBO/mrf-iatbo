<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));
class LoginAttributeRedirectPackage extends Package {

	protected $pkgHandle = 'login_attribute_redirect';
	protected $appVersionRequired = '5.4.0';
	protected $pkgVersion = '1.1.1';
	
	public function getPackageDescription() {
		return t("Redirects users on login based on an attribute value.");
	}
	
	public function getPackageName() {
		return t("Login Attribute Redirect");
	}
	
	public function install() {
		$pkg = parent::install();
		$pkg->saveConfig('LOGIN_ATTRIBUTE_REDIRECT_HANDLE', 'login_attribute_redirect_url');
		$pkg->saveConfig('LOGIN_ATTRIBUTE_REDIRECT_NEXT_HANDLE', 'login_attribute_redirect_next_url');
		$handle = $pkg->config('LOGIN_ATTRIBUTE_REDIRECT_HANDLE');
		Loader::model('user_attributes');
		$ak = UserAttributeKey::getByHandle($handle);
		$textt = AttributeType::getByHandle('text');
			
		if(!$ak instanceof UserAttributeKey) { 
			UserAttributeKey::add($textt, array('akHandle' => $handle, 'akName' => t('Redirect to on login'), 'akIsSearchable' => true), $pkg);
		}
		
		$handle = $pkg->config('LOGIN_ATTRIBUTE_REDIRECT_NEXT_HANDLE');
		$ak = UserAttributeKey::getByHandle($handle);
		if(!$ak instanceof UserAttributeKey) { 
			UserAttributeKey::add($textt, array('akHandle' => $handle, 'akName' => t('Redirect to on next login'), 'akIsSearchable' => true), $pkg);
		}		
	}
	
	public function upgrade() {
		$pkg = Package::getByHandle($this->pkgHandle);
		$pkg->saveConfig('LOGIN_ATTRIBUTE_REDIRECT_HANDLE', 'login_attribute_redirect_url');
		$pkg->saveConfig('LOGIN_ATTRIBUTE_REDIRECT_NEXT_HANDLE', 'login_attribute_redirect_next_url');
		
		Loader::model('user_attributes');
		$textt = AttributeType::getByHandle('text');
		$handle = $pkg->config('LOGIN_ATTRIBUTE_REDIRECT_NEXT_HANDLE');
		$ak = UserAttributeKey::getByHandle($handle);
		if(!$ak instanceof UserAttributeKey) { 
			UserAttributeKey::add($textt, array('akHandle' => $handle, 'akName' => t('Redirect to on next login'), 'akIsSearchable' => true), $pkg);
		}
		
		parent::upgrade();
	}
	
	public function on_start() { 
		$pkg = Package::getByHandle($this->pkgHandle);
		Events::extend('on_user_login',
				'LoginAttributeRedirect',
				'checkDoRedirect',
				'packages/'.$this->pkgHandle.'/models/login_attribute_redirect.php'
				);
		
	}
}