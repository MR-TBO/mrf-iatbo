<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class ScrapbookTabsPackage extends Package {

	protected $pkgHandle = 'scrapbook_tabs';
	protected $appVersionRequired = '5.3.2';
	protected $pkgVersion = '1.1';
	
	public function getPackageDescription() {
		return t("Created tabbed content based on a C5 Scrapbook.");
	}
	
	public function getPackageName() {
		return t("Scrapbook Tabs");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('scrapbook_tabs', $pkg);
		
	}




}