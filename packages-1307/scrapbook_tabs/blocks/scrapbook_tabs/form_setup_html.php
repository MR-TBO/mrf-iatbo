<?php  
$scrapbookHelper=Loader::helper('concrete/scrapbook');
$availableScrapbooks = $scrapbookHelper->getAvailableScrapbooks();
?>
<table border="0">
	<tr>
		<td>
			<strong><?php   echo t('Scrapbook')?></strong>
		</td>
		<td>
			<select name="scrapbook">
			<?php  
			foreach($availableScrapbooks as $availableScrapbook){
				echo '<option value="' . $availableScrapbook['arHandle'] . '"';
				if ($availableScrapbook['arHandle'] == $tabObj->scrapbook) {
					echo ' selected="selected" ';
				}
				echo '>' . $availableScrapbook['arHandle'] . '</option>';
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Transition')?></strong>
		</td>
		<td>
			<select name="transition">
				<option value="None" <?php   if ($tabObj->transition == 'None') { echo 'selected="selected"'; } ?>>None</option>
				<option value="Slide" <?php   if ($tabObj->transition == 'Slide') { echo 'selected="selected"'; } ?>>Slide Up</option>
				<option value="Fade" <?php   if ($tabObj->transition == 'Fade') { echo 'selected="selected"'; } ?>>Fade</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Change Tab On')?></strong>
		</td>
		<td>
			<select name="changeAction">
				<option value="click" <?php   if ($tabObj->changeAction == 'click') { echo 'selected="selected"'; } ?>>Click</option>
				<option value="mouseover" <?php   if ($tabObj->changeAction == 'mouseover') { echo 'selected="selected"'; } ?>>Mouseover</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Collapsible?')?></strong>
		</td>
		<td>
			<input name="collapsible" type="checkbox" value="1" <?php   echo ($tabObj->collapsible)?'checked="checked"':''?>>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Auto Rotate?')?></strong>
		</td>
		<td>
			<input name="autoRotate" type="checkbox" value="1" <?php   echo ($tabObj->autoRotate)?'checked="checked"':''?>>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php   echo t('Rotate Speed')?></strong>
		</td>
		<td>
			<input name="speed" type="text"  maxlength="6" size="10" value="<?php   echo $tabObj->speed; ?>" /> ms
		</td>
	</tr>
</table>