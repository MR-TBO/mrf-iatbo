<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$scrapbookHelper=Loader::helper('concrete/scrapbook');
$tabId = 'tabs' . intval($bID);
$tabContentClass = $tabId . '-content';

switch($transition) {
	case 'None':
		$fx = "";
		break;
	case 'Fade':
		$fx = "fx: { 
			opacity: 'toggle'
		},";
		break;
	case 'Slide':
		$fx = "fx: { 
			height: 'toggle'
		},";
		break;
}

if ($collapsible == '1') {
	$iscollapsible = 'true';
} else {
	$iscollapsible = 'false';
}

if ($autoRotate == '1') {
	$autoRotateJs = "$('#$tabId').tabs('rotate', $speed, true);";
} else {
	$autoRotateJs = '';
}

?>
<script type="text/javascript">
$(function(){
	$('#<?php   echo $tabId; ?>').tabs({
		<?php   echo $fx; ?>
		event: '<?php   echo $changeAction; ?>',
		collapsible: <?php   echo $iscollapsible; ?>	
	});
	<?php   echo $autoRotateJs; ?>
});
</script>
<?php  
	if( !count($scrapbookBlocks) ){
		echo t('No tabs to display.');
	} else {
		echo '<div id="' . $tabId . '">';
		echo '<ul>';
		
		//Loop through blocks to make tab titles
		$i=1;
		foreach($scrapbookBlocks as $b) {
			echo '<li><a href="#' . $tabId . '-' . $i . '">' . $b->getBlockName() . '</a></li>';
			$i++;
		}
		echo '</ul>';
		
		//Loop through blocks to make tab content
		$i=1;
		foreach($scrapbookBlocks as $b) {
			$bv = new BlockView();
			echo '<div id="' . $tabId . '-' . $i . '" class="' . $tabContentClass . '">';
			echo  $bv->render($b, 'scrapbook');
			echo '</div>';
			$i++;
		}
		echo '</div>';
	}
?>
