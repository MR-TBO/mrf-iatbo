<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

class ScrapbookTabsBlockController extends BlockController {
	
	var $pobj;
	
	protected $btTable = 'btScrapbookTabs';
	protected $btInterfaceWidth = "400";
	protected $btInterfaceHeight = "280";
	
	public $title = '';
	public $scrapbook = '';
	public $transition = '';
	public $speed = '';
	public $changeAction = '';
	public $collapsible = '';
	public $autoRotate = '';
	
	public function getBlockTypeDescription() {
		return t("Created tabbed content based on a C5 Scrapbook");
	}
	
	public function getBlockTypeName() {
		return t("Scrapbook Tabs");
	}
	
	function __construct($obj = null) {		
		parent::__construct($obj);	
		if(!$this->title) $this->title=t("My Tabbed Content");
	}
	
	function view(){ 
	
		$scrapbookHelper=Loader::helper('concrete/scrapbook');
		$html = Loader::helper('html');
		$this->addHeaderItem('test');
	
		$this->set('bID', $this->bID);	
		$this->set('scrapbook', $this->scrapbook);
		$this->set('transition', $this->transition);
		$this->set('speed', $this->speed);
		$this->set('changeAction', $this->changeAction);
		$this->set('collapsible', $this->collapsible);
		$this->set('autoRotate', $this->autoRotate);
		
		$c = $scrapbookHelper->getGlobalScrapbookPage();
		$cPath=$c->getCollectionPath();
		$this->set('scrapbookBlocks', $c->getBlocks($this->scrapbook));		
	}

	function save($data) {
		$args['scrapbook'] = $data['scrapbook'];
		$args['transition'] = $data['transition'];
		$args['speed'] = $data['speed'];
		$args['changeAction'] = $data['changeAction'];
		if ($data['collapsible'] == '1') {
			$args['collapsible'] = '1';
		} else {
			$args['collapsible'] = '0';
		}
		
		if ($data['autoRotate'] == '1') {
			$args['autoRotate'] = '1';
		} else {
			$args['autoRotate'] = '0';
		}
		
		parent::save($args);
	}
	
}

?>