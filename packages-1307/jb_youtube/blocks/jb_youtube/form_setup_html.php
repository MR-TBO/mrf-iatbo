<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); ?>  

<div class="ccm-block-field-group">
<?php  echo $form->label('content', 'Video URL');?>
<?php  echo $form->text('content', $content, array('style' => 'width: 340px; padding:2px'));?>
</div>
<div class="ccm-block-field-group">
<?php  echo $form->label('videowidth', 'Video Width (px)');?>
<?php  echo $form->text('videowidth', $videowidth, array('style' => 'width: 40px; padding:2px'));?> (numbers only)
		</div>