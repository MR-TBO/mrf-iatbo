<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

class JbYoutubePackage extends Package {

	protected $pkgHandle = 'jb_youtube';
	protected $appVersionRequired = '5.3.3.1';
	protected $pkgVersion = '1.0';

	public function getPackageDescription() {
		return t('YouTube Pro Video Player');
	}

	public function getPackageName() {
		return t('YouTube Pro');
	}

	public function install() {
		$pkg = parent::install();

		// install block
		BlockType::installBlockTypeFromPackage('jb_youtube', $pkg);

	}

}