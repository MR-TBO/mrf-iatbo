<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('sixeightdatadisplay','sixeightdatadisplay');

header('Expires: 0');
header('Cache-control: private');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');
header('Content-Type: text/xml');
header('Content-disposition: attachment; filename=export.xml');


$xmltext = "<?php  xml version=\"1.0\" encoding=\"UTF-8\"?>\n<data></data>";
$xml = simplexml_load_string($xmltext);

if(is_array($_GET['forms'])) {
	foreach($_GET['forms'] as $qsID) {
		$formInfo = Sixeightdatadisplay::getFormInfoByQSID($qsID);
		$formName = $formInfo['surveyName'];
		$formMsg = $formInfo['thankyouMsg'];
		$questions = Sixeightdatadisplay::getFormQuestions($qsID);
		$fxml = $xml->addChild('form');
		$fxml->addAttribute('name',$formName);
		$fxml->addChild('message',$formMsg);
		foreach($questions as $q) {
			$qxml = $fxml->addChild('field');
			$qxml->addAttribute('name',$q['question']);
			$qxml->addAttribute('type',$q['inputType']);
			$qxml->addAttribute('width',$q['width']);
			$qxml->addAttribute('height',$q['height']);
			if($q['options'] != '') {
				$options = explode('%%',$q['options']);
				if(is_array($options)) {
					foreach($options as $o) {
						$qxml->addChild('option',$o);	
					}
				}
			}
		}
	}
}

if(is_array($_GET['listTemplates'])) {
	foreach($_GET['listTemplates'] as $tID) {
		$template = Sixeightdatadisplay::getTemplate($tID);
		$txml = $xml->addChild('template');
		$txml->addAttribute('type','list');
		$txml->addAttribute('name',$template['templateName']);
		$txml->addChild('header',$template['templateHeader']);
		$txml->addChild('content',$template['templateContent']);
		$txml->addChild('alternate',$template['templateAlternateContent']);
		$txml->addChild('footer',$template['templateFooter']);
	}
}

if(is_array($_GET['detailTemplates'])) {
	foreach($_GET['detailTemplates'] as $tID) {
		$template = Sixeightdatadisplay::getTemplate($tID);
		$txml = $xml->addChild('template');
		$txml->addAttribute('type','detail');
		$txml->addAttribute('name',$template['templateName']);
		$txml->addChild('content',$template['templateContent']);
	}
}

$dom = dom_import_simplexml($xml)->ownerDocument;
$dom->formatOutput = true;
echo $dom->saveXML(); 
?>

