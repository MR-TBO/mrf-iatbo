<?php     

defined('C5_EXECUTE') or die(_("Access Denied."));
Loader::model('sixeightdatadisplay','sixeightdatadisplay');
Loader::model('form','sixeightforms');

class DashboardSixeightdatadisplayToolsController extends Controller {
	
	function view() {
		$c=$this->getCollectionObject();
		$this->set('listTemplates',Sixeightdatadisplay::getTemplates('list'));
		$this->set('detailTemplates',Sixeightdatadisplay::getTemplates('detail'));
		$this->set('forms',sixeightForm::getAll());
		
		if(Package::getByHandle('datadisplay')) {
			Loader::model('datadisplay','datadisplay');
			$this->set('oldListTemplates', DataDisplay::getTemplates('list'));
			$this->set('oldDetailTemplates', DataDisplay::getTemplates('detail'));
		}
	}
	
	public function createTemplate() {
		$id = Sixeightdatadisplay::createTemplate($_GET,'list');
		$this->redirect('/dashboard/datadisplay/list/-/getTemplate?tID=' . $id . '&new=true');
	}
	
	public function importFormData() {
		Loader::library('file/importer');
		$fi = new FileImporter();
		$resp = $fi->import($_FILES['data_file']['tmp_name'], $_FILES['data_file']['name'], $fr);

		if (!($resp instanceof FileVersion)) {
			switch($resp) {
				case FileImporter::E_FILE_INVALID_EXTENSION:
					$this->set('error', array(t('Invalid file extension.')));
					break;
				case FileImporter::E_FILE_INVALID:
					$this->set('error', array(t('Invalid file.')));
					break;
			}
		} else {
			$resp->getFileID();
			$filepath=$resp->getPath();  
			if (($handle = fopen($filepath, "r")) !== FALSE) {
			    while (($data = fgetcsv($handle)) !== FALSE) {
			        $num = count($data);
			        echo "<p> $num fields in line $row: <br /></p>\n";
			        $row++;
			        for ($c=0; $c < $num; $c++) {
			            echo $data[$c] . "<br />\n";
			        }
			    }
			    fclose($handle);
			}
		}
	}
	
	public function importData() {
		Loader::library('file/importer');
		$fi = new FileImporter();
		$resp = $fi->import($_FILES['data_file']['tmp_name'], $_FILES['data_file']['name'], $fr);

		if (!($resp instanceof FileVersion)) {
			switch($resp) {
				case FileImporter::E_FILE_INVALID_EXTENSION:
					$this->set('error', array(t('Invalid file extension.')));
					break;
				case FileImporter::E_FILE_INVALID:
					$this->set('error', array(t('Invalid file.')));
					break;
			}
		} else {
			$resp->getFileID();
			$filepath=$resp->getPath();  
			$filecontents = @file_get_contents($filepath);
			$xml = @simplexml_load_string($filecontents, 'SimpleXMLElement', LIBXML_NOCDATA);
			if(!$xml) {
				//Process errors
				$this->set('error', array(t('The file you uploaded does not appear to contain valid XML.')));
			} else {
				$formsPage = Page::getByPath('/dashboard/datadisplay/forms');
				//Process the XML file
				$bt = BlockType::getByHandle('form');
				$j=0;
				$formErrors = 0;
				$totalImportErrors = 0;
				foreach($xml->form as $form) {
					$invalidForm = 0;
					$data = array();
					$data['qsID']=time() + $j; //Adding $j to prevent duplicate qsID's if we happen to be importing more than one form
					$data['surveyName'] = $form->attributes()->name;
					if($data['surveyName'] == '') {
						$invalidForm++;
					}
					$data['notifyMeOnSubmission'] = 0;
					$data['thankyouMsg'] = $xml->form->message;
					$i=1;
					foreach($form->field as $field) {
						$options='';
						foreach($field->option as $option) {
							$options .= $option . "\n";
						}
						if($field->attributes()->name == '') {
							$invalidForm++;
						}
						if($field->attributes()->type == '') {
							$invalidForm++;
						}
						$data['questions'][] = array( 'qsID'=>$data['qsID'], 'question'=>$field->attributes()->name, 'inputType'=>$field->attributes()->type, 'options'=>trim($options), 'position'=>$i, 'required' => $field->attributes()->required, 'width' => $field->attributes()->width, 'height' => $field->attributes()->height);
						$i++;
					}
					
					if($invalidForm == 0) {
						$formsPage->addBlock($bt, "sixEight_formListArea", $data);
						$j++;
					} else {
						if($formErrors == 0) {
							$this->set('error', array(t('One or more forms in the XML file could not imported.  Please confirm that all forms have a name and that each field has a name and a type.')));
						}
						$formErrors++;
						$totalImportErrors++;
					}
				}
				
				$templateErrors = 0;
				foreach($xml->template as $template) {
					$invalidTemplate=0;
					if($template->attributes()->name == '') {
						$invalidTemplate++;
					}
					if($template->attributes()->type == '') {
						$invalidTemplate++;	
					}
					$templateAttributes = array('qsID'=>$data['qsID'],'templateName'=>$template->attributes()->name);
					$templateType = $template->attributes()->type;
					
					if($invalidTemplate == 0) {
						$tID = Sixeightdatadisplay::createTemplate($templateAttributes,$templateType);
						$templateData = array('templateName'=>$template->attributes()->name,'qsid'=>$data['qsID'],'templateHeader'=>trim($template->header),'templateContent'=>trim($template->content),'templateAlternateContent'=>trim($template->alternate),'templateFooter'=>trim($template->footer),'tID'=>$tID);
						Sixeightdatadisplay::saveTemplate($templateData);
					} else {
						if($templateErrors == 0) {
							$this->set('error', array(t('One or more templates in the XML file could not imported.  Please confirm that all templates have a name and type.')));
						}
						$templateErrors++;
						$totalImportErrors++;
					}
				}
				if($totalImportErrors == 0) {
					$this->redirect('/dashboard/sixeightdatadisplay/tools','importSuccess');
				}
			}
		}
	}
	
	public function importSuccess() {
		$this->set('message',t('Template set imported successfully'));
		$this->view();
	}
	
	public function convertTemplate() {
		Loader::model('datadisplay','datadisplay');
		$oldTemplate = DataDisplay::getTemplate(intval($_GET['tID']));
		$templateAttributes = array();
		$templateAttributes['fID'] = 0;
		$templateAttributes['templateName'] = $oldTemplate['templateName'];
		
		$tID = sixeightdatadisplay::createTemplate($templateAttributes,$oldTemplate['templateType']);
		
		$templateAttributes['templateHeader'] = $oldTemplate['templateHeader'];
		$templateAttributes['templateContent'] = $oldTemplate['templateContent'];
		$templateAttributes['templateAlternateContent'] = $oldTemplate['templateAlternateContent'];
		$templateAttributes['templateFooter'] = $oldTemplate['templateFooter'];
		$templateAttributes['templateEmpty'] = $oldTemplate['templateEmpty'];
		$templateAttributes['tID'] = $tID;
		
		sixeightdatadisplay::saveTemplate($templateAttributes);
		
		$this->redirect('/dashboard/sixeightdatadisplay/tools','convertTemplateSuccess');
	}
	
	public function convertTemplateSuccess() {
		$this->set('message',t('The template has been converted.'));
		$this->view();
	}
	
}

?>