<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
global $c;
$purl = $c->getCollectionHandle();

if($answerSet) {
	$dataDisplay = new SixeightdatadisplayBlockController;
	$detail = $dataDisplay->generateTemplateContent($detailTemplateContent,$questions,$answerSet);
	//Replace list URL placeholder			
	$detail = str_replace('{{LISTURL}}',DIR_REL . '/index.php?cID=' . intval($_GET['ref_cID']),$detail);
	$detail = str_replace('<?php  xml version="1.0"?>','',$detail);
	$detail = str_replace('<?php xml version="1.0"?>','',$detail);
	echo '<a href="'.$purl.'">'.$detail.'</a>';

	//If the user has access to edit form records, 
	if(($f->userCanEdit()) || ($f->ownerCanEdit())) {
	?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.edit-answer-link').click(function (e) {
			e.preventDefault();
			var href = $(this).attr('href');
			$.fn.dialog.open({
				width: 800,
				height: 500,
				modal: false,
				href: href,
				title: '<?php  echo t('Edit Record'); ?>'			
			});
		});
		
		$('.delete-answer-link').click(function (e) {
			e.preventDefault();
			var href = $(this).attr('href');
			$.fn.dialog.open({
				width: 340,
				height: 70,
				modal: false,
				href: href,
				title: '<?php  echo t('Delete Record'); ?>'			
			});
		});
	});
	</script>
	<?php  } ?>
<?php  } else { //No answer set found ?>
	<?php  echo $detailTemplateEmpty; ?>
<?php  } ?>