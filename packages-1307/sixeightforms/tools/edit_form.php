<?php  

/* Adjusted to use new model format on 11-26-10 */

defined('C5_EXECUTE') or die(_("Access Denied."));

$ch = Page::getByPath("/dashboard/sixeightforms/forms");
$chp = new Permissions($ch);
if (!$chp->canRead()) {
	die(_("Access Denied."));
}

Loader::library('view');
Loader::model('form','sixeightforms');
$h = Loader::helper('concrete/interface');
$uh = Loader::helper('concrete/urls');
$formsURL=View::url('/dashboard/sixeightforms/forms', 'updateForm');
$txt = Loader::helper('text');
?>

<script type="text/javascript">
	$(document).ready(function() {	
		$('.ccm-dialog-tabs li a').click(function(e) {
			e.preventDefault();
			$('.ccm-dialog-tabs li').removeClass('ccm-nav-active');
			$(this).parent().addClass('ccm-nav-active');
			$('.ccm-tab').hide();
			$($(this).attr('href')).show();
		});
		
		$('#requiredColor').ColorPicker({
			onSubmit: function(hsb, hex, rgb, cal) {
				$('#requiredColor').val(hex);
				cal.hide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		});
		
		$('#afterSubmit').change(function(e) {
			switch($(this).val()) {
				case 'thankyou':
					$('#afterSubmitRedirect').hide();
					$('#afterSubmitMessage').show();
					$('#afterSubmitURL').hide();
					break;
				case 'redirect':
					$('#afterSubmitRedirect').show();
					$('#afterSubmitMessage').hide();
					$('#afterSubmitURL').hide();
					break;
				case 'url':
					$('#afterSubmitRedirect').hide();					
					$('#afterSubmitMessage').hide();
					$('#afterSubmitURL').show();
					break;
			}
		});
			
<?php  
$form = sixeightForm::getByID(intval($_GET['fID']));
$fields = $form->getFields();
foreach($form->properties as $property => $value) {
?>
			$('#<?php  echo $property; ?>').val('<?php  echo str_replace("\r\n",'\n',str_replace("'","\'",$value)); ?>');
<?php  } ?>

	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		switch($('#afterSubmit').val()) {
			case 'thankyou':
				$('#afterSubmitRedirect').hide();
				$('#afterSubmitMessage').show();
				$('#afterSubmitURL').hide();
				break;
			case 'redirect':
				$('#afterSubmitRedirect').show();
				$('#afterSubmitMessage').hide();
				$('#afterSubmitURL').hide();
				break;
			case 'url':
				$('#afterSubmitRedirect').hide();					
				$('#afterSubmitMessage').hide();
				$('#afterSubmitURL').show();
				break;
		}
	});
</script>

<ul class="ccm-dialog-tabs">
<li class="ccm-nav-active"><a href="#tab-general"><?php  echo t('General'); ?></a></li>
<li><a href="#tab-submission"><?php  echo t('Submission'); ?></a></li>
<li><a href="#tab-other"><?php  echo t('Other Settings'); ?></a></li>
<?php  if(Package::getByHandle('sixeightdatadisplay')) { ?><li><a href="#tab-data-display"><?php  echo t('Data Display Integration'); ?></a></li><?php  } ?>
</ul>
<form id="newFormForm" action="<?php  echo $formsURL; ?>" method="POST">
<input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
	<table cellpadding="8" cellspacing="0" border="0" id="tab-general" class="ccm-tab">
		<tr>
			<td>
				<div><?php  echo t('Form Name'); ?></div>
			</td>
			<td>
				<input id="name" name="name" type="text" size="25" value="<?php  echo $_GET['newFormName']; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Send email notification?'); ?></div>
			</td>
			<td>
				<select name="sendMail" id="sendMail">
					<option value="">---</option>
					<option value="1"><?php  echo t('Yes'); ?></option>
					<option value="0"><?php  echo t('No'); ?></option>
				</select>
			</td>
		</tr>
		<tr class="row-email">
			<td>
				<div>Include form data in email?</div>
			</td>
			<td>
				<select name="sendData" id="sendData">
					<option value="">---</option>
					<option value="1"><?php  echo t('Yes'); ?></option>
					<option value="0"><?php  echo t('No'); ?></option>
				</select>
			</td>
		</tr>
		<tr class="row-email">
			<td>
				<div><?php  echo t('From Name'); ?></div>
			</td>
			<td>
				<input id="mailFrom" name="mailFrom" type="text" size="25" />
			</td>
		</tr>
		<tr class="row-email">
			<td>
				<div><?php  echo t('From Email Address'); ?></div>
			</td>
			<td>
				<input id="mailFromAddress" name="mailFromAddress" type="text" size="25" />
			</td>
		</tr>
		<tr class="row-email">
			<td>
				<div><?php  echo t('Send email to'); ?></div>
				<div class="ccm-note"><?php  echo t('Separate multiple addresses with commas.'); ?></div>
			</td>
			<td>
				<input id="mailTo" name="mailTo" type="text" size="25" />
			</td>
		</tr>
		<tr class="row-email">
			<td>
				<div><?php  echo t('Subject'); ?></div>
			</td>
			<td>
				<input id="mailSubject" name="mailSubject" type="text" size="25" />
			</td>
		</tr>
	</table>
	<table cellpadding="8" cellspacing="0" border="0" style="display:none" id="tab-submission" class="ccm-tab">
		<tr>
			<td>
				<div><?php  echo t('After submitting form:'); ?></div>
				<select name="afterSubmit" id="afterSubmit">
					<option value="thankyou"><?php  echo t('Display a message'); ?></option>
					<option value="redirect"><?php  echo t('Redirect to a page within this site'); ?></option>
					<option value="url"><?php  echo t('Redirect to a specific URL'); ?></option>
				</select>
			</td>
		</tr>
		<tr id="afterSubmitRedirect">
			<td>
				<div><?php  echo t('Page to redirect to:'); ?></div>
				<?php   $pageSelector = Loader::helper('form/page_selector');
				print $pageSelector->selectPage('thankyouCID',$form->properties['thankyouCID']);
				?>
			</td>
		</tr>
		<tr id="afterSubmitMessage">
			<td>
				<div><?php  echo t('Thank you message:'); ?></div>
				<div class="ccm-note">(<?php  echo t('HTML Capable'); ?>)</div>
				<textarea name="thankyouMsg" id="thankyouMsg" rows="6" cols="70" style="font-size:11px;"></textarea>
			</td>
		</tr>
		<tr id="afterSubmitURL">
			<td>
				<div><?php  echo t('URL:'); ?></div>
				<div class="ccm-note"><?php  echo t('Must start with'); ?> http://</div>
				<input type="text" size="50" id="thankyouURL" name="thankyouURL" />
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Use') ;?> CAPTCHA?</div>
				<select id="captcha" name="captcha">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Re-index search when form is submitted?'); ?></div>
				<select id="autoIndex" name="autoIndex">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
		<tr>
			<td>
				<div><?php  echo t('Submit label text:'); ?></div>
				<input id="submitLabel" name="submitLabel" type="text" size="25" value="Submit" />
			</td>
		</tr>
	</table>
	<table cellpadding="8" cellspacing="0" border="0" style="display:none" id="tab-other" class="ccm-tab">
		<tr>
			<td>
				<div><?php  echo t('Notify user upon approval?'); ?></div>
				<select name="sendApprovalNotification" id="sendApprovalNotification">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Payment Gateway'); ?></div>
				<select name="gateway" id="gateway">
					<option value="">---</option>
					<?php  foreach (sixeightform::getPaymentGateways() as $g) { ?>
						<option value="<?php  echo $g; ?>">
							<?php  if (strpos($g, '.') !== false) {
								print substr($txt->unhandle($g), 0, strrpos($g, '.'));
							} else {
								print $txt->unhandle($g);
							} ?>
						</option>
					<?php  } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Maximum Order Price'); ?></div>
				$<input id="maximumPrice" name="maximumPrice" type="text" size="6" />
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Message before Processing E-Commerce:'); ?></div>
				<div class="ccm-note">(<?php  echo t('HTML Capable'); ?>)</div>
				<textarea name="ecommerceConfirmation" id="ecommerceConfirmation" rows="6" cols="70" style="font-size:11px;"></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Required field indicator'); ?></div>
				<input id="requiredIndicator" name="requiredIndicator" type="text" size="6" value="*" />
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Required indicator color'); ?> (RGB)</div>
				&#35;<input id="requiredColor" name="requiredColor" type="text" size="6" value="#ff0000"/>
			</td>
		</tr>
	</table>
	<?php  if(Package::getByHandle('sixeightdatadisplay')) { ?>
	<table cellpadding="4" cellspacing="0" border="0" style="display:none" id="tab-data-display" class="ccm-tab">
		<tr>
			<td>
				<div><?php  echo t('Disable cache'); ?>?</div>
			</td>
			<td>
				<select name="disableCache" id="disableCache">
					<option value="0">No</option>
					<option value="1">Yes</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Create a new page when form is submitted?'); ?></div>
			</td>
			<td>
				<select name="autoCreatePage" id="autoCreatePage">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Default Detail Template'); ?></div>
			</td>
			<td>
				<select name="detailTemplateID" id="detailTemplateID">
				<?php  
					Loader::model('sixeightdatadisplay','sixeightdatadisplay');
					$detailTemplates = sixeightdatadisplay::getTemplates('detail');
					foreach($detailTemplates as $t) {
				?>
					<option value="<?php  echo $t['tID']; ?>"><?php  echo $t['templateName']; ?></option>
				<?php  } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Create page as child of:'); ?></div>
			</td>
			<td>
				<?php  
				$fh = Loader::helper('form/page_selector');
				print $fh->selectPage('parentCID', $form->properties['parentCID']);
				?>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Page Type'); ?></div>
			</td>
			<td>
				<select name="ctID" id="ctID">
				<?php  
					Loader::model('collection_types');
					$ctArray = CollectionType::getList();
					foreach($ctArray as $ct) {
				?>
					<option value="<?php  echo $ct->getCollectionTypeID(); ?>"><?php  echo $ct->getCollectionTypeName()?></option>
				<?php  } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Name'); ?></div>
			</td>
			<td>
				<select name="cName" id="cName">
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Alias'); ?></div>
			</td>
			<td>
				<select name="cHandle" id="cHandle">
					<option value="0">---</option>
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Description'); ?></div>
			</td>
			<td>
				<select name="cDescription" id="cDescription">
					<option value="0">---</option>
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Meta Title'); ?></div>
			</td>
			<td>
				<select name="meta_title" id="meta_title">
					<option value="0">---</option>
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Meta Keywords'); ?></div>
			</td>
			<td>
				<select name="meta_keywords" id="meta_keywords">
					<option value="0">---</option>
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Meta Description'); ?></div>
			</td>
			<td>
				<select name="meta_description" id="meta_description">
					<option value="0">---</option>
					<?php  if(count($fields) > 0) { foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } }?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div><?php  echo t('Exclude from Nav?'); ?></div>
			</td>
			<td>
				<select name="exclude_nav" id="exclude_nav">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
			</td>
		</tr>
	</table>
	<?php  } ?>
	<table cellpadding="8" cellspacing="0" border="0">
		<tr>
			<td>
				<?php  echo $h->button_js( t('Update Form'), 'validateFormName()','left'); ?>
			</td>
		</tr>
	</table>
</form>