<?php  
$h = Loader::helper('concrete/interface');
$uh = Loader::helper('concrete/urls');
$this->addHeaderItem($html->javascript('jquery.tablednd.js', 'sixeightforms'));
?>
<style type="text/css">
#fields-table {
	width:100%;
	font-size:11px;
}

.drag-row td {
	background-color:#ddd;
}

.sem-textarea {
	font-size:12px;
}

.new-field-link {
	padding:5px 0;
}

.new-field-link a {
	font-size:12px;
}
</style>
<script type="text/javascript">

$(document).ready(function() {
	$('#fields-table').tableDnD({
		onDragClass: 'drag-row',
		dragHandle: 'drag-handle',
		onDrop: function () {
			saveSortOrder();
			
		}
	});
});

function saveSortOrder() {
	var formData = $('#field-sort-form').serialize();
	$.ajax({
		type:'POST',
		url: '<?php  echo $uh->getToolsURL('savefieldsort','sixeightforms'); ?>',
		data: formData,
		success: function(msg) {	
		}
	});
	return false;
}

function validateFormName() {
	$('#newFormForm').submit();
}

function confirmDelete(fID) {
	if(confirm('<?php  echo t('Are you sure you want to delete this form and all of its responses?'); ?>')) {
		return true;
	} else {
		return false;
	}
}

function confirmDeleteField(ffID) {
	if(confirm('<?php  echo t('Are you sure you want to delete this field?'); ?>')) {
		window.location = '<?php  echo $this->url($c->getCollectionPath(),'deleteField'); ?>?ffID=' + ffID + '&fID=' + <?php  echo intval($_GET['fID']); ?>;
	} else {
		return false;
	}
}

function startNewForm() {
	$.fn.dialog.open({
		width: 500,
		height: 250,
		modal: false,
		href: '<?php  echo $uh->getToolsURL('new_form','sixeightforms'); ?>',
		title: '<?php  echo t('New Form'); ?>'			
	});
}

function addField() {
	$.fn.dialog.open({
		width: 400,
		height: 350,
		modal: false,
		href: '<?php  echo $uh->getToolsURL('new_field?fID=' . $_GET['fID'],'sixeightforms'); ?>',
		title: '<?php  echo t('Add Field'); ?>'
	});
}

function editField(ffID) {
	$.fn.dialog.open({
		width: 400,
		height: 350,
		modal: false,
		href: '<?php  echo $uh->getToolsURL('new_field?fID=' . $_GET['fID'],'sixeightforms'); ?>&ffID=' + ffID,
		title: '<?php  echo t('Edit Field'); ?>'
	});
}

function setNotifications(fID) {
	var href = '<?php  echo $uh->getToolsURL('notifications','sixeightforms') . '?fID='; ?>' + fID;
	$.fn.dialog.open({
		width: 500,
		height: 390,
		modal: false,
		href: href,
		title: '<?php  echo t('Notifications'); ?>'			
	});
}

function setPermissions(fID) {
	var href = '<?php  echo $uh->getToolsURL('permissions','sixeightforms') . '?fID='; ?>' + fID;
	$.fn.dialog.open({
		width: 500,
		height: 390,
		modal: false,
		href: href,
		title: '<?php  echo t('Permissions'); ?>'			
	});
}

function deleteForm(fID) {
	if(confirmDelete(fID)) {
		window.location = '<?php  echo $this->url($c->getCollectionPath(),'deleteForm','?fID='); ?>' + fID;
	}
}

$(document).ready(function() {
	$('.ccm-results-list tr:even').addClass('ccm-list-record-alt');
	
	$('.answer-detail-link').click(function (e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$.fn.dialog.open({
			width: 500,
			height: 390,
			modal: false,
			href: href,
			title: '<?php  echo t('Answer Details'); ?>'			
		});
	});
	
	$('.send-notification-link').click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$.fn.dialog.open({
			width: 300,
			height: 50,
			modal: false,
			href: href,
			title: '<?php  echo t('Send Notification'); ?>'			
		});
	});
	
	$('.edit-answer-link').click(function (e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$.fn.dialog.open({
			width: 800,
			height: 500,
			modal: false,
			href: href,
			title: '<?php  echo t('Edit Record'); ?>'			
		});
	});
	
	$('.settings-link').click(function (e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$.fn.dialog.open({
			width: 800,
			height: 500,
			modal: false,
			href: href,
			title: '<?php  echo t('Edit Form'); ?>'			
		});
	});
	
	$('.approval-button').click(function(e) {	
		$approvalButton = $(this);
		var asID = $approvalButton.attr('rel');
		var email = $approvalButton.attr('name');
		$approvalButton.html('<img src="<?php  echo DIR_REL; ?>/concrete/images/throbber_white_16.gif" />');
		e.preventDefault();
		$.ajax({
			url:'<?php  echo $uh->getToolsURL("update_approval","sixeightforms"); ?>?asID=' + $approvalButton.attr('rel'),
			success:function(data) {
				if(data == '1') {
					$approvalButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept.png" />');
					if(email != '') {
						if(confirm('<?php  echo t('A notification of approval will be sent to '); ?>' + email + '.\n<?php  echo t('Click OK to send.'); ?>')) {
							$.ajax({
								url:'<?php  echo $uh->getToolsURL("send_approval_notification","sixeightforms"); ?>?asID=' + $approvalButton.attr('rel'),
								success:function() {
									alert('<?php  echo t('Email sent!'); ?>');
								}
							});
						}
					}
				} else {
					$approvalButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept-off.png" />');
				}
			}
		});
	});
	
	$('.required-button').click(function(e) {
		$requiredButton = $(this);
		$requiredButton.html('<img src="<?php  echo DIR_REL; ?>/concrete/images/throbber_white_16.gif" />');
		e.preventDefault();
		$.ajax({
			url:'<?php  echo $uh->getToolsURL("update_required","sixeightforms"); ?>?ffID=' + $requiredButton.attr('rel'),
			success:function(data) {
				if(data == '1') {
					$requiredButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept.png" />');
				} else {
					$requiredButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept-off.png" />');
				}
			}
		});
	});
	
	$('.searchable-button').click(function(e) {
		$searchableButton = $(this);
		$searchableButton.html('<img src="<?php  echo DIR_REL; ?>/concrete/images/throbber_white_16.gif" />');
		e.preventDefault();
		$.ajax({
			url:'<?php  echo $uh->getToolsURL("update_searchable","sixeightforms"); ?>?ffID=' + $searchableButton.attr('rel'),
			success:function(data) {
				if(data == '1') {
					$searchableButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/search.png" />');
				} else {
					$searchableButton.html('<img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/search-off.png" />');
				}
			}
		});
	});
	
	$('#results-action').change(function(e) {
		if(($(this).val() == 'export') || ($(this).val() == 'summary')) {
			$('#results-range').val('all');
			$('#results-range').attr('disabled',true);
		} else {
			$('#results-range').attr('disabled',false);
		}
	});
	
	$('#sem-answer-select-1').change(function() {
		var ffID = $(this).val();
		$('.sem-results-row').each(function() {
			$('.sem-answer-td-1', this).html($('.answer-' + ffID, this).html());
		});
	});
	
	$('.new-field-link a').click(function(e) {
		e.preventDefault();
		var type = $(this).attr('rel');
		$.fn.dialog.open({
			width: 400,
			height: 300,
			modal: false,
			href: '<?php  echo $uh->getToolsURL('new_field','sixeightforms'); ?>?type=' + type + '&fID=<?php  echo $_GET['fID']; ?>',
			title: '<?php  echo t('Add Field'); ?>'
		});
	});
	
	$('.owner-link').dialog({ width: 900, title: 'Select Owner' });

	$('.reset-search-button').click(function(e) {
		e.preventDefault();
		window.location = '<?php  echo View::URL('/dashboard/sixeightforms/forms','results?fID=' . $_GET['fID']); ?>';
	});

});

var currentOwnerASID = 0;

function addRecord(fID) {
	currentFID = <?php  echo intval($_GET['fID']); ?>;
	if(currentFID != 0) {
		fID = currentFID;
	}
	$.fn.dialog.open({
		width: 800,
		height: 500,
		modal: false,
		href: '<?php  echo $uh->getToolsURL('edit_answerset','sixeightforms'); ?>?fID=' + fID,
		title: 'Add Record'			
	});
}

function indexRecords() {
	window.location = '<?php  echo $this->url($c->getCollectionPath(),'results'); ?>?fID=<?php  echo $_GET['fID']; ?>&index=1';
}

function clearCache() {
	window.location = '<?php  echo $this->url($c->getCollectionPath(),'results'); ?>?fID=<?php  echo $_GET['fID']; ?>&clearCache=1';
}

function sortByField() {
	var fID = <?php  echo intval($_GET['fID']); ?>;
	var q = '<?php  echo htmlentities($_GET['q']); ?>';
	var sortOrder = '<?php  echo $newSortOrder; ?>';
	var sortffID = $('#sem-answer-select-1').val();
	window.location = '<?php  echo $this->url($c->getCollectionPath(),'results'); ?>?fID=' + fID + '&q=' + q + '&sortOrder=' + sortOrder + '&sortffID=' + sortffID;
}

function ccm_triggerSelectUser(uID, uName) {
	$.ajax({
		url:'<?php  echo $uh->getToolsURL("set_answer_set_owner","sixeightforms"); ?>?asID=' + currentOwnerASID + '&uID=' + uID,
		success:function(data) {
			$('#owner-link-' + currentOwnerASID).html(uName);
		}
	});
}

</script>
<?php  if ($action == 'results') { //Display form results ?>

<h1><span><?php  echo t('Results'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<table>
		<tr>
			<td width="210" valign="top" class="ccm-search-form-advanced-col">
				<form method="get" action="<?php  echo $this->url($c->getCollectionPath(),'results'); ?>">
					<input type="hidden" name="fID" value="<?php  echo $_GET['fID']; ?>" />
					<div id="ccm-user-search-advanced-fields" class="ccm-search-advanced-fields" >
						<h2><?php  echo t('Search'); ?></h2>
						<?php  if($f->properties['indexTimestamp']) { ?>
							<div class="ccm-note"><?php  echo t('Index last updated'); ?> <?php  echo date('F j, Y, g:i a',$f->properties['indexTimestamp']); ?></div>
							<div id="ccm-search-advanced-fields-inner">
								<div class="ccm-search-field">
									<input type="text" name="q" value="<?php  echo htmlspecialchars($_GET['q']); ?>" style="width:200px" style="width:200px" class="ccm-input-text" />
								</div>
								<div id="ccm-search-fields-submit">
									<input type="submit" class="ccm-input-submit" value="<?php  echo t('Search'); ?>" />
									<input type="submit" class="reset-search-button" value="Reset Results">
								</div>
							</div>
						<?php  } else { ?>
							<?php  echo t('You must update the search index before you can search these records.'); ?>
						<?php  } ?>
						
					</div>
				</form>
			</td>
			<td valign="top">
				<div style="margin:10px 0;">
					<?php  echo $h->button_js( t('Add a Record'), 'addRecord();','left'); ?>
					<?php  echo $h->button_js( t('Update Search Index'), 'indexRecords();','left'); ?>
					<?php  echo $h->button_js( t('Clear Cache'), 'clearCache();','left'); ?>
					<?php  echo $h->button_js( t('Return to Form List'), 'window.location = \'' . $this->url("/dashboard/sixeightforms/forms") . '\'','left'); ?>
					<div style="clear:both;text-align:center"></div>
				</div>
				<form action="<?php  echo $this->url($c->getCollectionPath(),'processAnswerSets')?>" method="post">
				<input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
			
				<table cellpadding="8" cellspacing="0" border="0" class="ccm-results-list" id="sem-results-table">
					<tr>
						<th width="25" style="text-align:center"><input type="checkbox" onclick="$(this).is(':checked') ? $('.as-checkbox').attr('checked',true) : $('.as-checkbox').attr('checked',false);" /></th>
						<th width="200"><strong><a href="<?php  echo $this->url($c->getCollectionPath(),'results'); ?>?fID=<?php  echo intval($_GET['fID']); ?>&ffID=<?php  echo intval($_GET['ffID']); ?>&q=<?php  echo $_GET['q']; ?>&sortOrder=<?php  echo $newSortOrder; ?>"><?php  echo t('Date Submitted'); ?><img src="<?php  echo ASSETS_URL_IMAGES?>/icons/arrow_<?php  echo $arrowDirection; ?>.png" /></strong></th>
						<th><strong><?php  echo t('Owner'); ?></strong></th>
						<th class="sem-answer-th" id="sem-answer-th-1">
							<select id="sem-answer-select-1">
								<?php  foreach($fields as $field) { ?>
									<option <?php  if($field->ffID == $sortffID) { echo 'selected="selected"'; } ?> value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
									<?php  $i++; ?>
								<?php  } ?>
							</select>
							<strong><a onclick="sortByField();" href="javascript:void(0);"><img src="<?php  echo ASSETS_URL_IMAGES?>/icons/arrow_<?php  echo $arrowDirection; ?>.png" /></a></strong>
						</th>
						<th width="50" style="text-align:center"><strong><?php  echo t('View'); ?></strong></th>
						<th width="50" style="text-align:center"><strong><?php  echo t('Send'); ?></strong></th>
						<th width="50" style="text-align:center"><strong><?php  echo t('Approve'); ?></strong></th>
						<th width="50" style="text-align:center"><strong><?php  echo t('Edit'); ?></strong></th>
						<th width="50" style="text-align:center"><strong><?php  echo t('Delete'); ?></strong></th>
					</tr>
					<?php   
					if(is_array($answerSets)) {
						foreach($answerSets as $as) {
					?>
					<tr class="ccm-list-record sem-results-row">
						<td style="text-align:center">
							<input type="checkbox" class="as-checkbox" name="sem-as[]" value="<?php  echo $as->asID; ?>" />
							<?php   foreach($fields as $field) { ?>
								<div class="answer-<?php  echo $field->ffID; ?>" style="display:none"><?php  echo $as->answers[$field->ffID]['shortValue']; ?></div>
							<?php   } ?>
						</td>
						<td><?php  echo date('F j, Y, g:i a',$as->dateSubmitted); ?></td>
						<td style="text-align:center">
							<a dialog-modal="false" onclick="currentOwnerASID=<?php  echo $as->asID; ?>;" id="owner-link-<?php  echo $as->asID; ?>" class="owner-link" href="<?php  echo $uh->getToolsURL('users/search_dialog?mode=choose_one'); ?>">
							<?php  
								$uName = $as->getOwnerUserName();
								if($uName) {
									echo $uName;
								} else {
									echo '---';
								}
							?>
							</a>
						</td>
						<td class="sem-answer-td-1"><?php  echo $as->answers[$fields[$sortffID]->ffID]['shortValue']; ?></td>
						<td style="text-align:center">
							<a class="answer-detail-link" href="<?php  echo $uh->getToolsURL('answer_detail','sixeightforms') . '?asID=' . intval($as->asID); ?>"><img src="<?php  echo ASSETS_URL_IMAGES; ?>/icons/search.png" /></a>
						</td>
						<td style="text-align:center"><a class="send-notification-link" href="<?php  echo $uh->getToolsURL('send_notification_prompt','sixeightforms') . '?asID=' . intval($as->asID); ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/email.png" /></a></td>
						<td style="text-align:center">
						<?php  
							if($f->properties['sendApprovalNotification'] == 1) {
								if($asUI = UserInfo::getByID($as->creator)) {
									$asEmail = $asUI->getUserEmail();
								}
							} else {
								$asEmail = '';
							}
						?>
						<?php   if($as->isApproved == 1) { ?>
							<a href="#" class="approval-button" rel="<?php  echo $as->asID; ?>" name="<?php  echo $asEmail; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept.png" /></a>
						<?php   } else { ?>
							<a href="#" class="approval-button" rel="<?php  echo $as->asID; ?>" name="<?php  echo $asEmail; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept-off.png" /></a>
						<?php   } ?>
						</td>
						<td style="text-align:center"><a href="<?php  echo $uh->getToolsURL('edit_answerset','sixeightforms') . '?asID=' . intval($as->asID) . '&editCode=' . $as->editCode; ?>" class="edit-answer-link"><img src="<?php  echo ASSETS_URL_IMAGES; ?>/icons/edit_small.png" /></a></td>
						<td style="text-align:center"><a onclick="if(!confirm('<?php  echo t('Are you sure you want to delete this record?'); ?>')) {return false;}" href="<?php  echo $this->url($c->getCollectionPath(),'deleteAnswerSet'); ?>?fID=<?php  echo $_GET['fID']; ?>&asID=<?php  echo $as->asID; ?>"><img src="<?php  echo ASSETS_URL_IMAGES; ?>/icons/delete_small.png" /></a></td>
					</tr>
					<?php   
						}
					} 
					?>
					<tr>
						<th colspan="10" style="text-align:center" valign="top">
							<div style="float:left">
								<select name="action" id="results-action">
									<option value="">---</option>
									<option value="delete"><?php  echo t('Delete'); ?></option>
									<option value="approve"><?php  echo t('Approve'); ?></option>
									<option value="export"><?php  echo t('Export Results'); ?></option>
									<option value="summary"><?php  echo t('Export Summary'); ?></option>
								</select>
								<select name="range" id="results-range">
									<option value="selected"><?php  echo t('selected answers'); ?></option>
									<option value="all"><?php  echo t('all answers'); ?></option>
								</select>
								<input type="submit" value="Go" />
							</div>
						</th>
					</tr>
					
				</table>
				</form>
				<br />
				<div style="float:left;">
					<form id="pageSizeForm" method="get">
						<input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
						<input type="hidden" name="pageNum" value="<?php  echo intval($_GET['pageNum']); ?>" />
						<input type="hidden" name="q" value="<?php  echo $_GET['q']; ?>" />
			        	<input type="hidden" name="sortOrder" value="<?php  echo $sortOrder; ?>" />
						<input type="hidden" name="sortffID" value="<?php  echo intval($_GET['sortffID']); ?>" />
						<select name="pageSize" onchange="$('#pageSizeForm').submit();">
							<option value="10" <?php   if($pageSize == 10) { echo 'selected="selected"'; } ?> >10</option>
							<option value="25" <?php   if($pageSize == 25) { echo 'selected="selected"'; } ?>>25</option>
							<option value="50" <?php   if($pageSize == 50) { echo 'selected="selected"'; } ?>>50</option>
							<option value="100" <?php   if($pageSize == 100) { echo 'selected="selected"'; } ?>>100</option>
							<option value="250" <?php   if($pageSize == 250) { echo 'selected="selected"'; } ?>>250</option>
							<option value="500" <?php   if($pageSize == 500) { echo 'selected="selected"'; } ?>>500</option>
						</select> <?php  echo t('results per page'); ?>
					</form>
				</div>
				<div style="float:right;">
				    <form id="pageNumForm" method="get">
				        <input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
				        <input type="hidden" name="ffID" value="<?php  echo intval($_GET['ffID']); ?>" />
				        <input type="hidden" name="q" value="<?php  echo htmlspecialchars($_GET['q']); ?>" />
				        <input type="hidden" name="sortOrder" value="<?php  echo $sortOrder; ?>" />
						<input type="hidden" name="sortffID" value="<?php  echo intval($_GET['sortffID']); ?>" />
						<?php   if (intval($_GET['pageSize']) == 0) { $_GET['pageSize'] = 25; } ?>
						<input type="hidden" name="pageSize" value="<?php  echo intval($_GET['pageSize']); ?>" />
				        Page 
						<select name="pageNum" onchange="$('#pageNumForm').submit();">
							<?php   for($i=1;$i<=$numPages;$i++) { ?>
							<option value="<?php  echo $i; ?>" <?php   if($_GET['pageNum'] == $i) { echo 'selected="selected"'; } ?> ><?php  echo $i; ?></option>
							<?php   } ?>
						</select>
				    </form>
				</div>
				<div style="clear:both"></div>
			</td>
		</tr>
	</table>
</div>

<?php   } elseif ($_GET['fID'] == '') { //Display form list ?>

<h1><span>Advanced Forms</span></h1>
<div class="ccm-dashboard-inner">
	<div style="margin:10px 0;">
		<?php  echo $h->button_js( t('Create a New Form'), 'startNewForm()','left'); ?>
			<div style="clear:both"></div>
	</div>
	<hr />
	<?php  if(count($forms) > 0) { ?>
		<table border="0" id="ccm-template-list">
		<?php  foreach($forms as $f) { ?>
			<tr>
				<td>
					<h2>
						<a href="<?php  echo $uh->getToolsURL('edit_form','sixeightforms') . '?fID=' . $f->fID; ?>" class="settings-link">
							<?php  if($f->properties['name']) { ?>
								<?php  echo $f->properties['name']; ?>
							<?php  } else { ?>
								Form <?php  echo $f->fID; ?>
							<?php  } ?>
						</a>
					</h2>
					<?php  echo t('Created ') . date('F j, Y, g:i a',$f->properties['dateCreated']); ?> (<?php  echo $f->getTotalAnswerSetCount(); ?> <?php  echo t('records'); ?>)<br /><br />
					<?php  echo $h->button(t('Records'),$this->url($c->getCollectionPath(),'results','?fID=' . $f->fID),'left'); ?>
					<?php  echo $h->button(t('Fields'),$this->url($c->getCollectionPath(),'manageFields','?fID=' . $f->fID),'left'); ?>
					<?php  echo $h->button_js(t('Notifications'),'setNotifications(' . $f->fID . ');','left'); ?>
					<?php  echo $h->button_js(t('Permissions'),'setPermissions(' . $f->fID . ');','left'); ?>
					<?php  echo $h->button(t('Duplicate'),$this->url($c->getCollectionPath(),'duplicateForm','?fID=' . $f->fID),'left'); ?>
					<?php  echo $h->button_js(t('Delete'),'deleteForm(' . $f->fID . ');','left'); ?>
					<br /><br />
				</td>
			</tr>
		<?php  } ?>
		</table>
	<?php  } else { ?>
		<?php  echo t('You have not yet created any forms.'); ?>
	<?php  } ?>
</div>

<?php   } else { //Display field list ?>

<h1><span><?php  echo $f->properties['name']; ?></span></h1>
<div class="ccm-dashboard-inner">
	<table width="100%">
		<tr>
			<td width="210" valign="top" class="ccm-search-form-advanced-col">
				<form method="get" action="<?php  echo $this->url($c->getCollectionPath(),'results'); ?>">
					<div id="ccm-user-search-advanced-fields" class="ccm-search-advanced-fields" >
						<h2><?php  echo t('Add New Field'); ?></h2>
						<div id="ccm-search-advanced-fields-inner">
							<div class="ccm-search-field">
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/textfield.png" align="absmiddle" /> <a href="#" rel="sem-text-single-line">Text (Single-Line)</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/textarea.png" align="absmiddle" /> <a href="#" rel="sem-text-multi-line">Text (Multi-Line)</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/number.png" align="absmiddle" /> <a href="#" rel="sem-number">Number</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/email.png" align="absmiddle" /> <a href="#" rel="sem-email-address">Email Address</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/dropdown.png" align="absmiddle" /> <a href="#" rel="sem-dropdown">Dropdown</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/multi_select.png" align="absmiddle" /> <a href="#" rel="sem-multi-select">Multi-Select</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/radio.png" align="absmiddle" /> <a href="#" rel="sem-radio-button">Radio Button</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/checkbox.png" align="absmiddle" /> <a href="#" rel="sem-checkbox">Checkbox</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/date.png" align="absmiddle" /> <a href="#" rel="sem-date">Date</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/time.png" align="absmiddle" /> <a href="#" rel="sem-time">Time</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/file_upload.png" align="absmiddle" /> <a href="#" rel="sem-file-upload">File Upload</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/file_manager.png" align="absmiddle" /> <a href="#" rel="sem-file-from-file-manager">File from File Manager</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/wysiwyg.png" align="absmiddle" /> <a href="#" rel="sem-wysiwyg">WYSIWYG</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/sellable_item.png" align="absmiddle" /> <a href="#" rel="sem-sellable-item">Sellable Item</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/credit_card.png" align="absmiddle" /> <a href="#" rel="sem-credit-card">Credit Card</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/hidden.png" align="absmiddle" /> <a href="#" rel="sem-hidden">Hidden</a></div>
								<div class="new-field-link"><img src="<?php   echo DIR_REL; ?>/packages/sixeightforms/images/text.png" align="absmiddle" /> <a href="#" rel="sem-text-no-user-input">Text (no user input)</a></div>
							</div>
						</div>
					</div>
				</form>
			</td>
			<td valign="top">
				<div style="margin:10px 0;">
					<?php  echo $h->button_js( t('Return to Form List'), 'window.location = \'' . $this->url("/dashboard/sixeightforms/forms") . '\'','left'); ?>
						<div style="clear:both"></div>
				</div>
				<hr />
				<h2>Fields</h2>
				<?php  
				if(is_array($fields)) {
				?>
				<form id="field-sort-form" action="<?php  echo $this->url($c->getCollectionPath(),'processFields'); ?>" method="post">
				<input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
				<table cellpadding="4" cellspacing="0" border="0" width="100%" class="ccm-results-list" id="fields-table">
					<tr class="nodrag nodrop">
						<th width="20"><input type="checkbox" onclick="$(this).is(':checked') ? $('.ff-checkbox').attr('checked',true) : $('.ff-checkbox').attr('checked',false);" /></th>
						<th width="20"><?php  echo t('Sort'); ?></th>
						<th width="50"><?php  echo t('Required'); ?></th>
						<th width="50"><?php  echo t('Searchable'); ?></th>
						<th width="20">&nbsp;</th>
						<th style="text-align:center"><strong><?php  echo t('Label'); ?></strong></th>
						<th style="text-align:center" width="20"><strong><?php  echo t('Edit'); ?></strong></th>
						<th style="text-align:center" width="20"><strong><?php  echo t('Delete'); ?></strong></th>
					</tr>
					<?php   foreach($fields as $field) { ?>
					<tr class="ccm-list-record">
						<td style="text-align:center"><input type="checkbox" class="ff-checkbox" name="sem-ff[]" value="<?php  echo $field->ffID; ?>" /></td>
						<td style="text-align:center" class="drag-handle">
							<img src="<?php  echo ASSETS_URL_IMAGES?>/icons/up_down.png" style="cursor:move" />
							<input type="hidden" name="sort[]" value="<?php  echo $field->ffID; ?>" />
						</td>
						<td style="text-align:center">
							<?php   if($field->isRequired()) { ?>
								<a href="#" class="required-button" rel="<?php  echo $field->ffID; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept.png" /></a></td>
							<?php   } else { ?>
								<a href="#" class="required-button" rel="<?php  echo $field->ffID; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/accept-off.png" /></a></td>
							<?php   } ?>
						<td style="text-align:center">
							<?php  if($field->indexable) { ?>
								<a href="#" class="searchable-button" rel="<?php  echo $field->ffID; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/search.png" /></a></td>
							<?php  } else { ?>
								<a href="#" class="searchable-button" rel="<?php  echo $field->ffID; ?>"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/search-off.png" /></a></td>
							<?php  } ?>
						</td>
						<td><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/<?php  echo $field->getFieldTypeHandle(); ?>.png" align="absmiddle"  /></td>
						<td>
			            	<strong style="font-size:12px"><?php  echo $field->shortLabel; ?></strong><br />
			                <span style="font-size:11px"><?php  echo $field->type; ?></span>
			            </td>
						<td style="text-align:center"><a href="javascript:void(0);" onclick="editField(<?php  echo $field->ffID; ?>);"><img src="<?php  echo ASSETS_URL_IMAGES; ?>/icons/edit_small.png" /></a></td>
						<td style="text-align:center"><a href="javascript:void(0)" onclick="confirmDeleteField(<?php  echo $field->ffID; ?>)"><img src="<?php  echo ASSETS_URL_IMAGES; ?>/icons/delete_small.png" /></a></td>
					</tr>
					<?php   } ?>
					<tr>
						<td  colspan="8">
							<select name="action">
								<option value="">---</option>
								<option value="duplicate"><?php  echo t('Duplicate'); ?></option>
								<option value="delete"><?php  echo t('Delete'); ?></option>
							</select> <b><?php  echo t('selected fields'); ?></b>
							<input type="submit" value="<?php  echo t('Go'); ?>" />
						</td>
					</tr>
				<?php   } ?>
				</table>
				</form>
			</td>
		</tr>
	</table>
</div>
<?php   } ?>