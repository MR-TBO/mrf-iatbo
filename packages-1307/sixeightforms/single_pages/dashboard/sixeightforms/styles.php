<?php  
$h = Loader::helper('concrete/interface');
$uh = Loader::helper('concrete/urls');
?>
<script type="text/javascript">
function createStyle() {
	if($('#newStyleName').val() != '') {
		$('#new-style-form').submit();
	} else {
		alert('Please specify a style name.');
	}
}

function loadStyle() {
	if($('#sID').val() != 0) {
		location.href = '<?php   echo $this->url($c->getCollectionPath(),'loadStyle')?>?sID=' + $('#sID').val();
	} else {
		alert('<?php   echo t('Please select a style'); ?>');
	}
}

function duplicateStyle() {
	if($('#sID').val() != 0) {
		location.href = '<?php   echo $this->url($c->getCollectionPath(),'duplicateStyle')?>?sID=' + $('#sID').val();
	} else {
		alert('<?php   echo t('Please select a style'); ?>');
	}
}

function deleteStyle() {
	if($('#sID').val() != 0) {	
		if(confirm("<?php   echo t('Are you sure you want to delete this style? This action cannot be undone!').'\n' ?>")) {
			location.href = '<?php   echo $this->url($c->getCollectionPath(),'deleteStyle')?>?sID=' + $('#sID').val();
		}
	} else {
		alert('<?php   echo t('Please select a style'); ?>');
	}
}

function saveSelector(ssID) {
	$('#changed-indicator-' + ssID).html('<span><img src="<?php   echo DIR_REL; ?>/concrete/images/throbber_white_16.gif" /></span>');
	$.ajax({
		url:'<?php   echo $uh->getToolsURL("save_selector","sixeightforms"); ?>?ssID=' + ssID,
		type:'post',
		data:'ssID=' + ssID + '&css=' + $('#selector-css-' + ssID).val(),
		success:function(data) {
			$('#changed-indicator-' + ssID).html('*').hide();
		}
	});
}

</script>
<?php   if($_GET['sID'] == '') { ?>
<h1><span><?php   echo t('Create a Style'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<form id="new-style-form" action="<?php   echo $this->url($c->getCollectionPath(),'createStyle')?>" method="post">
		<table cellpadding="8" cellspacing="0" border="0">
			<tr>
				<td>
					<div><?php   echo t('Style Name'); ?></div>
					<input id="newStyleName" name="name" type="text" size="25" /><br />
					<input type="checkbox" name="useTemplate" value="1" checked="checked" /> <?php  echo t('Use default form style as template'); ?>
				</td>
				<td>
					<?php   echo $h->button_js( t('Create Style'), 'createStyle()','left'); ?>
				</td>
			</tr>
		</table>
	</form>
</div>

<h1><span><?php   echo t('Existing Styles'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<?php   if(is_array($styles)) { ?>
	<table cellpadding="8" cellspacing="0" border="0">
			<tr>
				<td>
					<?php   echo t('Select a Style'); ?><br />
					<select id="sID" name="sID">
						<option value="0">---</option>
						<?php   foreach($styles as $style) { ?>
							<option value="<?php   echo $style->sID; ?>"><?php   echo $style->name; ?></option>
						<?php   } ?>
					</select>
				</td>
				<td>
					<?php   echo $h->button_js( t('Edit Style'), 'loadStyle()','left'); ?>
					<?php   echo $h->button_js( t('Duplicate Style'), 'duplicateStyle()','left'); ?>
					<?php   echo $h->button_js( t('Delete Style'), 'deleteStyle()','left'); ?>
				</td>
			</tr>
		</table>
	<?php   } else { ?>
		<?php   echo t('Use the form above to create a new style.'); ?>
	<?php   } ?>
</div>
<?php   } else { ?>
<h1><span>Edit Style</span></h1>
<div class="ccm-dashboard-inner">
	<form id="style-form" action="<?php   echo $this->url($c->getCollectionPath(),'saveStyle')?>" method="post">
	<input type="hidden" name="sID" value="<?php   echo intval($_GET['sID']); ?>" />
	<?php   if(is_array($style->selectors)) { ?>
		<?php   foreach($style->selectors as $selector) { ?>
			<table class="entry-form" border="0" cellspacing="1" cellpadding="0" width="100%">
				<tr>
					<td colspan="2" class="subheader">
						<?php   echo $h->button_js(t('Save'), 'saveSelector(' . $selector['ssID'] . ');','left'); ?>
						<?php   echo $selector['name']; ?><span id="changed-indicator-<?php   echo $selector['ssID']; ?>" style="display:none;color:#ff0000">*</span>
						<div class="ccm-note"><?php   echo $selector['description']; ?></div>
					</td>
				</tr>
				<tr>
					<td><textarea onkeyup="$('#changed-indicator-<?php   echo $selector['ssID']; ?>').show();" style="width:100%;height:200px" name="selector-css[<?php   echo $selector['ssID']; ?>]" id="selector-css-<?php   echo $selector['ssID']; ?>"><?php   echo $selector['css']; ?></textarea></td>
				</tr>
			</table>
		<?php   } ?>
	<?php   } ?>
	<?php   echo $h->button_js(t('Save All'), "$('#style-form').submit();",'left'); ?>
	<div style="clear:both"></div>
	</form>
</div>
<?php   } ?>