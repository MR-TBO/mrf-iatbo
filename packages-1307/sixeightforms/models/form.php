<?php  
	class sixeightForm extends Object {
	
		public function getByID($fID) {
			$db = Loader::db();
			$f = new sixeightForm;
			$f->fID = $fID;
			$row = $db->getRow("SELECT * FROM sixeightforms WHERE fID=?", array($fID));
			if ($row['fID'] == $fID) {
				foreach($row as $col => $val) {
					$f->properties[$col] = $val;
				}
			}
			
			//If no payment gateway is set, use paypal by default
			if($f->properties['gateway'] == '') {
				$f->properties['gateway'] == 'paypal';
			}
			
			return $f;
		}
		
		public function getAll() {
			$db = Loader::db();
			$formsData = $db->getAll("SELECT fID FROM sixeightforms WHERE isDeleted != 1");
			if(count($formsData) > 0) {
				$forms = array();
				foreach($formsData as $formRow) {
					$forms[] = sixeightForm::getByID($formRow['fID']);
				}
			}
			return $forms;
		}
		
		public function create($data) {
			$db = Loader::db();
			$sql = array();
			$sqlData[] = $data['name'];
			$sqlData[] = $data['sendMail'];
			$sqlData[] = $data['sendData'];
			$sqlData[] = $data['mailFrom'];
			$sqlData[] = $data['mailFromAddress'];
			$sqlData[] = $data['mailTo'];
			$sqlData[] = $data['mailCc'];
			$sqlData[] = $data['mailBcc'];
			$sqlData[] = $data['mailSubject'];
			$sqlData[] = $data['afterSubmit'];
			$sqlData[] = $data['thankyouCID'];
			$sqlData[] = $data['thankyouMsg'];
			$sqlData[] = $data['thankyouURL'];
			$sqlData[] = $data['captcha'];
			$sqlData[] = $data['submitLabel'];
			$sqlData[] = $data['fieldLabelLocation'];
			$sqlData[] = $data['sendApprovalNotification'];
			$sqlData[] = $data['requiredIndicator'];
			$sqlData[] = $data['requiredColor'];
			$sqlData[] = $data['gateway'];
			$sqlData[] = $data['ecommerceConfirmation'];
			$sqlData[] = $data['defaultAnswerSetStatus'];
			$sqlData[] = $data['autoCreatePage'];
			$sqlData[] = $data['parentCID'];
			$sqlData[] = $data['ctID'];
			$sqlData[] = $data['cName'];
			$sqlData[] = $data['cHandle'];
			$sqlData[] = $data['cDescription'];
			$sqlData[] = $data['meta_title'];
			$sqlData[] = $data['meta_description'];
			$sqlData[] = $data['meta_keywords'];
			$sqlData[] = $data['exclude_nav'];
			$sqlData[] = $data['detailTemplateID'];
			$sqlData[] = $data['disableCache'];
			$sqlData[] = $data['maximumPrice'];
			$sqlData[] = $data['autoIndex'];
			
			$db->execute("INSERT INTO sixeightforms (fID, dateCreated, name, sendMail, sendData, mailFrom, mailFromAddress, mailTo, mailCc, mailBcc, mailSubject, afterSubmit, thankyouCID, thankyouMsg, thankyouURL, captcha, submitLabel, fieldLabelLocation, sendApprovalNotification, requiredIndicator, requiredColor, gateway, ecommerceConfirmation, defaultAnswerSetStatus, autoCreatePage, parentCID, ctID, cName, cHandle, cDescription, meta_title, meta_description, meta_keywords, exclude_nav, detailTemplateID, disableCache, maximumPrice, autoIndex) VALUES (0, '" . time() . "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",$sqlData);
			$f = sixeightForm::getByID($db->Insert_ID());
			$f->setDefaultPermissions();
			return $f;
		}
		
		public function update($data) {
			$db = Loader::db();
			
			$sqlData[] = $data['name'];
			$sqlData[] = $data['sendMail'];
			$sqlData[] = $data['sendData'];
			$sqlData[] = $data['mailFrom'];
			$sqlData[] = $data['mailFromAddress'];
			$sqlData[] = $data['mailTo'];
			$sqlData[] = $data['mailCc'];
			$sqlData[] = $data['mailBcc'];
			$sqlData[] = $data['mailSubject'];
			$sqlData[] = $data['afterSubmit'];
			$sqlData[] = $data['thankyouCID'];
			$sqlData[] = $data['thankyouMsg'];
			$sqlData[] = $data['thankyouURL'];
			$sqlData[] = $data['captcha'];
			$sqlData[] = $data['submitLabel'];
			$sqlData[] = $data['fieldLabelLocation'];
			$sqlData[] = $data['sendApprovalNotification'];
			$sqlData[] = $data['requiredIndicator'];
			$sqlData[] = $data['requiredColor'];
			$sqlData[] = $data['gateway'];
			$sqlData[] = $data['ecommerceConfirmation'];
			$sqlData[] = $data['defaultAnswerSetStatus'];
			$sqlData[] = $data['autoCreatePage'];
			$sqlData[] = $data['parentCID'];
			$sqlData[] = $data['ctID'];
			$sqlData[] = $data['cName'];
			$sqlData[] = $data['cHandle'];
			$sqlData[] = $data['cDescription'];
			$sqlData[] = $data['meta_title'];
			$sqlData[] = $data['meta_description'];
			$sqlData[] = $data['meta_keywords'];
			$sqlData[] = $data['exclude_nav'];
			$sqlData[] = $data['detailTemplateID'];
			$sqlData[] = $data['disableCache'];
			$sqlData[] = $data['maximumPrice'];
			$sqlData[] = $data['autoIndex'];
			$sqlData[] = $this->fID;
			
			$db->execute("UPDATE sixeightforms SET name = ?, sendMail = ?, sendData = ?, mailFrom = ?, mailFromAddress = ?, mailTo = ?, mailCc = ?, mailBcc = ?, mailSubject = ?, afterSubmit = ?, thankyouCID = ?, thankyouMsg = ?, thankyouURL = ?, captcha = ?, submitLabel = ?, fieldLabelLocation = ?, sendApprovalNotification = ?, requiredIndicator = ?, requiredColor = ?, gateway = ?, ecommerceConfirmation = ?, defaultAnswerSetStatus = ?, autoCreatePage = ?, parentCID = ?, ctID = ?, cName = ?, cHandle = ?, cDescription = ?, meta_title = ?, meta_description = ?, meta_keywords = ?, exclude_nav = ?, detailTemplateID = ?, disableCache = ?, maximumPrice = ?, autoIndex = ? WHERE fID=?",$sqlData);
		}
		
		public function duplicate() {
			$data = array();
			$data['name'] = $this->properties['name'];
			$data['sendMail'] = $this->properties['sendMail'];
			$data['sendData'] = $this->properties['sendData'];
			$data['mailFrom'] = $this->properties['mailFrom'];
			$data['mailFromAddress'] = $this->properties['mailFromAddress'];
			$data['mailTo'] = $this->properties['mailTo'];
			$data['mailCc'] = $this->properties['mailCc'];
			$data['mailBcc'] = $this->properties['mailBcc'];
			$data['mailSubject'] = $this->properties['mailSubject'];
			$data['afterSubmit'] = $this->properties['afterSubmit'];
			$data['thankyouCID'] = $this->properties['thankyouCID'];
			$data['thankyouMsg'] = $this->properties['thankyouMsg'];
			$data['thankyouURL'] = $this->properties['thankyouURL'];
			$data['captcha'] = $this->properties['captcha'];
			$data['submitLabel'] = $this->properties['submitLabel'];
			$data['fieldLabelLocation'] = $this->properties['fieldLabelLocation'];
			$data['sendApprovalNotification'] = $this->properties['sendApprovalNotification'];
			$data['requiredIndicator'] = $this->properties['requiredIndicator'];
			$data['requiredColor'] = $this->properties['requiredColor'];
			$data['gateway'] = $this->properties['gateway'];
			$data['ecommerceConfirmation'] = $this->properties['ecommerceConfirmation'];
			$data['defaultAnswerSetStatus'] = $this->properties['defaultAnswerSetStatus'];
			$data['autoCreatePage'] = $this->properties['autoCreatePage'];
			$data['parentCID'] = $this->properties['parentCID'];
			$data['ctID'] = $this->properties['ctID'];
			$data['cName'] = $this->properties['cName'];
			$data['cHandle'] = $this->properties['cHandle'];
			$data['cDescription'] = $this->properties['cDescription'];
			$data['meta_title'] = $this->properties['meta_title'];
			$data['meta_description'] = $this->properties['meta_description'];
			$data['meta_keywords'] = $this->properties['meta_keywords'];
			$data['exclude_nav'] = $this->properties['exclude_nav'];
			$data['detailTemplateID'] = $this->properties['detailTemplateID'];
			$data['disableCache'] = $this->properties['disableCache'];
			$data['maximumPrice'] = $this->properties['maximumPrice'];
			$data['autoIndex'] = $this->properties['autoIndex'];
			
			$newForm = sixeightForm::create($data);

			$fields = $this->getFields();
			foreach($fields as $field) {
				$field->duplicate($newForm->fID);
			}
			
		}
		
		public function delete() {
			$db = Loader::db();
			$db->execute("UPDATE sixeightforms SET isDeleted=1 WHERE fID=?",array($this->fID));
		}
		
		public function addField($data,$options='') {
			$data['fID'] = $this->fID;
			$field = sixeightField::create($data,$options);
			return $field;
		}
		
		public function getFieldCount() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND isDeleted != 1", array($this->fID));
			return $fieldCount['total'];
		}
		
		public function getFields() {
			Loader::model('field','sixeightforms');
			$db = Loader::db();
			$fieldsData = $db->getAll("SELECT ffID FROM sixeightformsFields WHERE fID = ? AND isDeleted != 1 ORDER by sortPriority ASC, ffID",array($this->fID));
			$fields = array();
			foreach($fieldsData as $fieldData) {
				$fields[$fieldData['ffID']] = sixeightField::getByID($fieldData['ffID']);
			}
			return $fields;
		}
		
		public function getTotalAnswerSetCount() {
			$db = Loader::db();
			$asCount = $db->getRow("SELECT count(asID) as total FROM sixeightformsAnswerSets WHERE fID=? AND isDeleted != 1", array($this->fID));
			return $asCount['total'];
		}
		
		public function saveConfirmation($ffID,$from,$subject,$message) {
			$db = Loader::db();
			$db->execute("UPDATE sixeightforms SET confirmationField=?, confirmationFrom=?, confirmationSubject=?, confirmationEmail=? WHERE fID=?",array($ffID,$from,$subject,$message,$this->fID));
		}
		
		public function clearNotifications() {
			$db = Loader::db();
			$db->execute("DELETE FROM sixeightformsNotifications WHERE fID=?",array($this->fID));
		}
		
		public function saveNotification($ffID,$conditionType,$value,$sendData,$email) {
			$db = Loader::db();
			$db->execute("INSERT INTO sixeightformsNotifications (nID,fID,ffID,conditionType,value,sendData,email) VALUES (0,?,?,?,?,?,?)",array($this->fID,$ffID,$conditionType,$value,$sendData,$email));
		}
		
		public function getNotifications() {
			$db = Loader::db();
			$notifications = $db->getAll("SELECT * FROM sixeightformsNotifications WHERE fID = ? ORDER by nID ASC",array($this->fID));
			return $notifications;
		}
		
		public function clearAnswersCache() {
			$db = Loader::db();
			$db->execute("DELETE FROM sixeightformsAnswersCache WHERE fID=?",array($this->fID));
			$db->execute("UPDATE sixeightformsAnswerSets SET matchingFilters='' WHERE fID=?",array($this->fID));
		}
		
		public function sendMail($to,$from='',$fromAddress='',$subject,$body) {
			$adminUserInfo=UserInfo::getByID(USER_SUPER_ID);
			if($from == '') {
				$from = SITE;
			}
			if($fromAddress == '') {
				$fromAddress = $adminUserInfo->getUserEmail(); 
			}
			$mh = Loader::helper('mail');
			$mh->to($to); 
			$mh->from($fromAddress, $from);
			$mh->setSubject($subject);
			$mh->setBody($body);
			@$mh->sendMail();
		}
		
		public function getTotalAnswerSets() {
			$db = Loader::db();
			$asCount = $db->getRow("SELECT count(asID) as total FROM sixeightformsAnswerSets WHERE fID=? AND isDeleted != 1", array($this->fID));
			return $asCount['total'];
		}
		
		public function getAnswerSetCount($requireApproval=0,$filterFields='',$filterValues='',$searchableFields='',$searchQuery='',$sortBy='',$sortOrder='ASC',$pageNum=1,$pageSize=999999,$showExpired=0,$ownedOnly=false) {
			$db = Loader::db();
			$initialSQLFields = array();
			$initialSQLFields[] = $this->fID;
			
			//Sanitize page number
			$pageNum = intval($pageNum);
			
			//Setup sort order SQL
			if($sortOrder == 'RAND') {
				$sortOrderSQL = 'ASC';
			} else {
				$sortOrderSQL = $sortOrder;
			}
			
			//If we are filtering, we need to set that up first
			if(($filterFields != '') && ($filterValues != '')) {
				
				$fcIDs = array(); //This array will be used when getting the answer sets in the main query
			
				$filterFields = explode(',',$filterFields);
				$filterValues = explode(',',$filterValues);
				
				//Add each field/value pair to the filter cache table and get filter cache ID's
				foreach($filterFields as $key=>$ffID) {
					$fcRow = $db->getRow('SELECT fcID FROM sixeightformsFilterCache WHERE ffID=? AND value=?',array($ffID,$filterValues[$key]));
					if(intval($fcRow['fcID']) == 0) { //This filter has not been cached
						$db->execute('INSERT INTO sixeightformsFilterCache (fcID,ffID,value) VALUES (0,?,?)',array($ffID,$filterValues[$key]));
						$newFcID = intval($db->Insert_ID());
					} else {
						$newFcID = intval($fcRow['fcID']);
					}
					
					//Get the answer set ID's for the records that match
					$filteredAnswerSets = $db->getAll('SELECT asID FROM sixeightformsAnswers WHERE ffID=? AND value LIKE ?',array($ffID,'%' . $filterValues[$key] . '%'));
					if(is_array($filteredAnswerSets)) {
						foreach($filteredAnswerSets as $as) {
							$asRow = $db->getRow('SELECT count(asID) as total FROM sixeightformsAnswerSets WHERE asID=? AND matchingFilters LIKE ?',array($as['asID'],'%:' . $newFcID . ':%'));
							if(intval($asRow['total']) == 0) {
								$db->execute('UPDATE sixeightformsAnswerSets SET matchingFilters=CONCAT_WS(":",matchingFilters,?) WHERE asID=?',array($newFcID . ':',$as['asID']));
							}
						}
					}
					
					$fcIDs[] = $newFcID;
				}
				
			}
			
			//Setup filter cache SQL
			$filterCacheSQL = '';
			if(is_array($fcIDs)) {
				foreach($fcIDs as $fcID) {
					$filterCacheSQL .= "AND matchingFilters LIKE '%:" . $fcIDs[0] . ":%'";
				}
			}
			
			//Setup approval SQL
			$approvalSQL = '';
			if($requireApproval == 1) {
				$approvalSQL = 'AND isApproved = 1';
			}
			
			//Setup Ownership SQL
			if($ownedOnly) {
				$owner = new User();
				$ownershipSQL = "AND creator = " . intval($owner->getUserID());
			}
			
			//Setup Expiration SQL
			if($showExpired == 0) {
				$expiredSQL = "AND (expiration > '" . time() . "' OR expiration IS NULL)";
			}
			
			//Setup Search SQL
			if($searchQuery != '') {
				$queryWords = explode(' ',$searchQuery);
				if(count($queryWords) == 1) {
					$searchSQL = "AND searchIndex LIKE ?";
					$initialSQLFields[] = '%' . $searchQuery . '%';
				} else {
					$searchSQL = "AND (";
					$i = 0;
					foreach($queryWords as $word) {
						if($i > 0) {
							$searchSQL .= ' AND ';
						}
						$searchSQL .= "searchIndex LIKE ?";
						$initialSQLFields[] = '%' . $word . '%';
						$i++;
					}
					$searchSQL .= ')';
				}
				
			}
			
			if(intval($pageSize) == 0) {
				$pageSize = 9999999;
			}
			
			$startAt = ($pageNum - 1) * $pageSize;
			if($startAt < 0) {
				$startAt = 0;
			}
			
			//If no sort is specified, sort by timestamp
			if(intval($sortBy) == 0) {
				$asQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $expiredSQL $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCacheQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCache = $db->getRow("SELECT asData FROM sixeightformsAnswersCache WHERE fID=? AND asQuery=? AND asQueryVars=? AND asSort=?",array($this->fID,$asCacheQuery,json_encode($initialSQLFields),$sortOrder));
				if($asCache['asData'] != '') {
					return count(json_decode($asCache['asData'],true));
				} else {
					$answerSets = $db->getAll($asQuery,$initialSQLFields);
				}
			} else {
				$initialSQLFields[] = $sortBy;
				$asQuery = "SELECT ans.asID, ans.dateSubmitted, ans.ipAddress, ans.isApproved, ans.editCode, a.value as sortValue FROM sixeightformsAnswerSets ans, sixeightformsAnswers a WHERE ans.fID = ? AND a.ffID=? AND a.asID = ans.asID AND ans.isDeleted != 1 $expiredSQL $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY a.value $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCacheQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCache = $db->getRow("SELECT asData FROM sixeightformsAnswersCache WHERE fID=? AND asQuery=? AND asQueryVars=? AND asSort=?",array($this->fID,$asCacheQuery,json_encode($initialSQLFields),$sortOrder));
				if($asCache['asData'] != '') {
					return count(json_decode($asCache['asData'],true));
				} else {
					$answerSets = $db->getAll($asQuery,$initialSQLFields);
				}
			}
			
			/*  No need to shuffle if the only point is to count the answer sets
			if (($sortOrder == 'RAND') && (is_array($answerSets))) {
				shuffle($answerSets);
			}
			*/
			
			/* No need to create indexed array if the only point is to count the answer sets
			$answerSetsById = array();
			foreach($answerSets as $as) {
				$answerSetsById[$as['asID']] = $as;
			}
			*/
			
			//Cache the Answer Set
			sixeightForm::addAnswerSetCache($asCacheQuery,$initialSQLFields,$answerSets,$sortOrder);
			return count($answerSets);
			
		}
		
		public function deleteAllAnswerSets() {
			$db = Loader::db();
			$answerSets = $db->execute("SELECT asID FROM sixeightformsAnswerSets WHERE fID=?",array($this->fID));
			foreach($answerSets as $as) {
				$db->execute("DELETE FROM sixeightformsAnswerSets WHERE asID=?",array($as['asID']));
				$db->execute("DELETE FROM sixeightformsAnswers WHERE asID=?",array($as['asID']));
			}
			$this->clearAnswersCache();
		}
		
		public function getAnswerSets($requireApproval=0,$filterFields='',$filterValues='',$searchableFields='',$searchQuery='',$sortBy='',$sortOrder='ASC',$pageNum=1,$pageSize=999999,$showExpired=0,$ownedOnly=false) {
			$db = Loader::db();
			$initialSQLFields = array();
			$initialSQLFields[] = $this->fID;
			
			//Sanitize page num
			$pageNum = intval($pageNum);
			
			//Setup sort order SQL
			if(intval($sortBy) != 0) {
				$initialSQLFields[] = intval($sortBy);
				$sortFF = sixeightField::getByID($sortBy);
				if($sortFF->type == 'Number') {
					$sortType = '(a.value + 0)';
				} else {
					$sortType = 'a.value';
				}
			} else {
				$sortType = 'a.value';
			}
			if($sortOrder == 'RAND') {
				$sortOrderSQL = 'ASC';
			} else {
				$sortOrderSQL = $sortOrder;
			}
			
			//If we are filtering, we need to set that up first
			if(($filterFields != '') && ($filterValues != '')) {
				
				$fcIDs = array(); //This array will be used when getting the answer sets in the main query
			
				$filterFields = explode(',',$filterFields);
				$filterValues = explode(',',$filterValues);
				
				//Add each field/value pair to the filter cache table and get filter cache ID's
				foreach($filterFields as $key=>$ffID) {
					$fcRow = $db->getRow('SELECT fcID FROM sixeightformsFilterCache WHERE ffID=? AND value=?',array($ffID,$filterValues[$key]));
					if(intval($fcRow['fcID']) == 0) { //This filter has not been cached
						$db->execute('INSERT INTO sixeightformsFilterCache (fcID,ffID,value) VALUES (0,?,?)',array($ffID,$filterValues[$key]));
						$newFcID = intval($db->Insert_ID());
					} else {
						$newFcID = intval($fcRow['fcID']);
					}
					
					//Get the answer set ID's for the records that match
					$filteredAnswerSets = $db->getAll('SELECT asID FROM sixeightformsAnswers WHERE ffID=? AND value LIKE ?',array($ffID,'%' . $filterValues[$key] . '%'));
					if(is_array($filteredAnswerSets)) {
						foreach($filteredAnswerSets as $as) {
							$asRow = $db->getRow('SELECT count(asID) as total FROM sixeightformsAnswerSets WHERE asID=? AND matchingFilters LIKE ?',array($as['asID'],'%:' . $newFcID . ':%'));
							if(intval($asRow['total']) == 0) {
								$db->execute('UPDATE sixeightformsAnswerSets SET matchingFilters=CONCAT_WS(":",matchingFilters,?) WHERE asID=?',array($newFcID . ':',$as['asID']));
							}
						}
					}
					
					$fcIDs[] = $newFcID;
				}
				
			}
			
			//Setup filter cache SQL
			$filterCacheSQL = '';
			if(is_array($fcIDs)) {
				foreach($fcIDs as $fcID) {
					$filterCacheSQL .= "AND matchingFilters LIKE '%:" . $fcIDs[0] . ":%'";
				}
			}
			
			//Setup approval SQL
			$approvalSQL = '';
			if($requireApproval == 1) {
				$approvalSQL = 'AND isApproved = 1';
			}
			
			//Setup Ownership SQL
			if($ownedOnly) {
				$owner = new User();
				$ownershipSQL = "AND creator = " . intval($owner->getUserID());
			}
			
			//Setup Expiration SQL
			if($showExpired == 0) {
				$expiredSQL = "AND (expiration > '" . (time() - 86400)  . "' OR expiration IS NULL)";
			}
			
			//Setup Search SQL
			if($searchQuery != '') {
				$queryWords = explode(' ',$searchQuery);
				if(count($queryWords) == 1) {
					$searchSQL = "AND searchIndex LIKE ?";
					$initialSQLFields[] = '%' . $searchQuery . '%';
				} else {
					$searchSQL = "AND (";
					$i = 0;
					foreach($queryWords as $word) {
						if($i > 0) {
							$searchSQL .= ' AND ';
						}
						$searchSQL .= "searchIndex LIKE ?";
						$initialSQLFields[] = '%' . $word . '%';
						$i++;
					}
					$searchSQL .= ')';
				}
				
			}
			
			if(intval($pageSize) == 0) {
				$pageSize = 9999999;
			}
			
			$startAt = ($pageNum - 1) * $pageSize;
			if($startAt < 0) {
				$startAt = 0;
			}
			
			//If no sort is specified, sort by timestamp
			if(intval($sortBy) == 0) {
				$asQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID, creator FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $expiredSQL $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCacheQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCache = $db->getRow("SELECT asData FROM sixeightformsAnswersCache WHERE fID=? AND asQuery=? AND asQueryVars=? AND asSort=?",array($this->fID,$asCacheQuery,json_encode($initialSQLFields),$sortOrder));
				if($asCache['asData'] != '') {
					$asArray = json_decode($asCache['asData'],true);
					if(count($asArray) > 0) {
						$answerSetsByID = array();
						foreach($asArray as $as) {
							$answerSetsByID[$as['asID']] = sixeightAnswerSet::getFromArray($as);
						}
						return $answerSetsByID;
					}
				} else {
					$answerSets = $db->getAll($asQuery,$initialSQLFields);
				}
			} else {
				$asQuery = "SELECT ans.asID, ans.dateSubmitted, ans.ipAddress, ans.isApproved, ans.editCode, ans.cID, ans.creator, a.value as sortValue FROM sixeightformsAnswerSets ans, sixeightformsAnswers a WHERE ans.fID = ? AND a.ffID=? AND a.asID = ans.asID AND ans.isDeleted != 1 $expiredSQL $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY $sortType $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCacheQuery = "SELECT asID, dateSubmitted, ipAddress, isApproved, editCode, cID FROM sixeightformsAnswerSets WHERE fID = ? AND isDeleted != 1 $approvalSQL $ownershipSQL $searchSQL $filterCacheSQL ORDER BY dateSubmitted $sortOrderSQL LIMIT $startAt, $pageSize";
				$asCache = $db->getRow("SELECT asData FROM sixeightformsAnswersCache WHERE fID=? AND asQuery=? AND asQueryVars=? AND asSort=?",array($this->fID,$asCacheQuery,json_encode($initialSQLFields),$sortOrder));
				if($asCache['asData'] != '') {
					$asArray = json_decode($asCache['asData'],true);
					if(count($asArray) > 0) {
						$answerSetsByID = array();
						foreach($asArray as $as) {
							$answerSetsByID[$as['asID']] = sixeightAnswerSet::getFromArray($as);
						}
						return $answerSetsByID;
					}
				} else {
					$answerSets = $db->getAll($asQuery,$initialSQLFields);
				}
				$answerSets = $db->getAll($asQuery,$initialSQLFields);
			}
			
			if (($sortOrder == 'RAND') && (is_array($answerSets))) {
				shuffle($answerSets);
			}
			
			$answerSetsByID = array();
			if(count($answerSets) > 0) {
				foreach($answerSets as $as) {
					$answerSetsByID[$as['asID']] = sixeightAnswerSet::getByID($as['asID']);
				}
			}
			
			//Cache the Answer Set
			sixeightForm::addAnswerSetCache($asCacheQuery,$initialSQLFields,$answerSetsByID,$sortOrder);
			return $answerSetsByID;
			
		}
		
		public function addAnswerSetCache($query,$queryVars,$answerSet,$sortOrder) {
			$db = Loader::db();
			if($this->properties['disableCache'] != 1) {
				$asQueryVars = json_encode($queryVars);
				$asData = json_encode($answerSet);
				$db->execute("INSERT INTO sixeightformsAnswersCache (acID, fID, asQuery, asQueryVars, asData, asSort) VALUES (0,?,?,?,?,?)",array($this->fID,$query,$asQueryVars,$asData,$sortOrder));
			}
		}
		
		public function hasSiteMapField() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND toolbar=1 AND isDeleted != 1", array($this->fID));
			if($fieldCount['total'] > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function hasFileManagerField() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND (type='File Upload' OR type='File from File Manager') AND isDeleted != 1",array($this->fID));
			if($fieldCount['total'] > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function hasWYSIWYGField() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND type='WYSIWYG' AND isDeleted != 1",array($this->fID));
			if($fieldCount['total'] > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function hasDateField() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND type='Date' AND isDeleted != 1",array($this->fID));
			if($fieldCount['total'] > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function hasCommerceField() {
			$db = Loader::db();
			$fieldCount = $db->getRow("SELECT count(ffID) as total FROM sixeightformsFields WHERE fID=? AND type='Sellable Item' AND isDeleted != 1", array($this->fID));
			if($fieldCount['total'] > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function getPaymentGateways() {
			$fh = Loader::helper('file');
			$templates = array();
			
			if (file_exists(DIR_FILES_ELEMENTS . '/sixeightforms/payment')) {
				$templates = array_merge($templates, $fh->getDirectoryContents(DIR_FILES_ELEMENTS . '/sixeightforms/payment'));
			}
			
			if(file_exists(DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment')) {
				$templates = array_merge($templates, $fh->getDirectoryContents(DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment'));
			}
			
			sort($templates);
			
			return $templates;
		}
		
		function indexAnswerSets() {
			$db = Loader::db();
			
			//Clear the cache so that when the user searches, they will get correct results
			$this->clearAnswersCache();
			
			//Get rid of the old search index for this form
			$db->execute("UPDATE sixeightformsAnswerSets SET searchIndex='' WHERE fID=?",array($this->fID));
			
			//Loop through the indexable fields for this form
			$fields = $db->getAll("SELECT ffID FROM sixeightformsFields WHERE fID=? AND indexable=1",array($this->fID));
			foreach($fields as $field) {
				//Loop through the answers for each field
				$answers = $db->getAll("SELECT asID,value FROM sixeightformsAnswers WHERE ffID=?",array($field['ffID']));
				foreach($answers as $answer) {
					$index[$answer['asID']] .= strip_tags($answer['value']) . ' ';
				}
			}
			
			if(count($index) > 0) {
				foreach($index as $asID => $data) {
					$db->execute("UPDATE sixeightformsAnswerSets SET searchIndex=? WHERE asID=?",array($data,$asID));
				}
				$db->execute("UPDATE sixeightforms SET indexTimestamp=? WHERE fID=?",array(time(),$this->fID));
			}
		}
		
		public function createAnswerSet($totalPrice='',$timestamp='',$approvalStatus=0,$cID=0) {
			$as = sixeightAnswerSet::create($this->fID,$totalPrice,$timestamp,$approvalStatus,$cID);
			return $as;
		}
		
		public function setPermissions($gID,$permissions) {
			$db = Loader::db();
			$this->resetGroupPermissions($gID);
			if(count($permissions) > 0) {
				$db->execute("INSERT INTO sixeightformsPermissions (fpID, fID, gID) VALUES (0,?,?)",array($this->fID,$gID));
				foreach($permissions as $permission) {
					$db->execute("UPDATE sixeightformsPermissions SET $permission = 1 WHERE fID = ? AND gID = ?",array($this->fID,$gID)); 
				}
			}			
		}
		
		public function resetPermissions() {
			$db = Loader::db();
			$db->execute("DELETE FROM sixeightformsPermissions WHERE fID = ?",array($this->fID));
		}
		
		public function resetGroupPermissions($gID) {
			$db = Loader::db();
			$db->execute("DELETE FROM sixeightformsPermissions WHERE fID = ? AND gID = ?",array($this->fID,$gID));
		}
		
		public function setDefaultPermissions() {
			$db = Loader::db();
			$this->resetPermissions();
			
			//Give guests permission to add records - This query assumes guests are group ID 1
			$db->execute("INSERT INTO sixeightformsPermissions (fpID, fID, gID, addRecords) VALUES (0, ?, 1, 1)",array($this->fID));
			
			//Give administrators full permissions - This query assumes guests are group ID 3
			$db->execute("INSERT INTO sixeightformsPermissions (fpID, fID, gID, addRecords, editRecords, deleteRecords, approveRecords) VALUES (0, ?, 3, 1, 1, 1, 1)",array($this->fID));
		}
		
		public function setOwnerCanEdit($status) {
			$db = Loader::db();
			if($status) {
				$db->execute("UPDATE sixeightforms SET ownerCanEdit=1 WHERE fID=?",array($this->fID));
			} else {
				$db->execute("UPDATE sixeightforms SET ownerCanEdit=0 WHERE fID=?",array($this->fID));
			}
		}
		
		public function ownerCanEdit() {
			if(intval($this->properties['ownerCanEdit']) == 1) {
				return true;
			} else {
				return false;
			}
		}
		
		public function setOwnerCanDelete($status) {
			$db = Loader::db();
			if($status) {
				$db->execute("UPDATE sixeightforms SET ownerCanDelete=1 WHERE fID=?",array($this->fID));
			} else {
				$db->execute("UPDATE sixeightforms SET ownerCanDelete=0 WHERE fID=?",array($this->fID));
			}
		}
		
		public function ownerCanDelete() {
			if(intval($this->properties['ownerCanDelete']) == 1) {
				return true;
			} else {
				return false;
			}
		}
		
		public function setOneRecordPerUser($status) {
			$db = Loader::db();
			if($status) {
				$db->execute("UPDATE sixeightforms SET oneRecordPerUser=1 WHERE fID=?",array($this->fID));
			} else {
				$db->execute("UPDATE sixeightforms SET oneRecordPerUser=0 WHERE fID=?",array($this->fID));
			}
		}
		
		public function oneRecordPerUser() {
			if(intval($this->properties['oneRecordPerUser']) == 1) {
				return true;
			} else {
				return false;
			}
		}
		
		public function groupCanAddRecords($gID) {
			$db = Loader::db();
			$gp = $db->getRow("SELECT * FROM sixeightformsPermissions WHERE fID = ? AND gID = ? AND addRecords = 1",array($this->fID,$gID));
			if(count($gp) > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function groupCanEditRecords($gID) {
			$db = Loader::db();
			$gp = $db->getRow("SELECT * FROM sixeightformsPermissions WHERE fID = ? AND gID = ? AND editRecords = 1",array($this->fID,$gID));
			if(count($gp) > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function groupCanDeleteRecords($gID) {
			$db = Loader::db();
			$gp = $db->getRow("SELECT * FROM sixeightformsPermissions WHERE fID = ? AND gID = ? AND deleteRecords = 1",array($this->fID,$gID));
			if(count($gp) > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function groupCanApproveRecords($gID) {
			$db = Loader::db();
			$gp = $db->getRow("SELECT * FROM sixeightformsPermissions WHERE fID = ? AND gID = ? AND approveRecords = 1",array($this->fID,$gID));
			if(count($gp) > 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public function userCanAdd() {
			$u = new User();
			
			//Check whether or not "One Record Per User" is set
			if($this->oneRecordPerUser()) {
				if(intval($u->getUserID()) == 0) {
					return false;
				} else {
					if($this->getUserAnswerSetCount() > 0) {
						return false;
					}
				}
			} else { //If "One Record Per User" is not set, the only other limiting factor is standard permissions
				if($u->isSuperUser()) { //Super user can always add
					return true;
				}
				
				foreach($u->uGroups as $gID => $gName) { //Loop through the groups
					if($this->groupCanAddRecords($gID)) { //If user is part of a group that can add, they can add
						return true;
					}
				}
			}
			
			return false; //Deny access by default
		}
		
		public function userCanEdit() {
			$u = new User();
			if($u->isSuperUser()) { //Super user can always add
				return true;
			}
			
			foreach($u->uGroups as $gID => $gName) { //Loop through the groups
				if($this->groupCanEditRecords($gID)) { //If user is part of a group that can add, they can add
					return true;
				}
			}
			
			return false;
		}
		
		public function userCanApprove() {
			$u = new User();
			
			if($u->isSuperUser()) { //Super user can always approve
				return true;
			}
			
			foreach($u->uGroups as $gID => $gName) {
				if($this->groupCanApproveRecords($gID)) {
					return true;
				}
			}
			
			return false; //Deny access by default
		}
		
		public function getUserAnswerSetCount() {
			$db = Loader::db();
			$u = new User();
			$asCount = $db->getRow("SELECT count(asID) AS total FROM sixeightformsAnswerSets WHERE fID=? AND creator=?",array($this->fID,intval($u->getUserID())));
			return $asCount['total'];
		}
		
		public function getGatewayPath() {
			if($this->properties['gateway'] == '') {
				$gateway = 'paypal';
			} else {
				$gateway = $this->properties['gateway'];
			}
			
			//Check root elements path first
			if (file_exists(DIR_FILES_ELEMENTS . '/sixeightforms/payment/' . $gateway)) {
				return DIR_FILES_ELEMENTS . '/sixeightforms/payment/' . $gateway;
			}
			
			if(file_exists(DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment/' . $gateway)) {
				return DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment/' . $gateway;
			}
			
			return false;
		}
		
		public function loadGatewayConfig() {
			$gp = $this->getGatewayPath();
			include($gp . '/config.php');
			$gateway = new semGateway();
			return $gateway;
		}
		
		public function loadGatewayForm($vars) {
			if($this->properties['gateway'] == '') {
				$gateway = 'paypal';
			} else {
				$gateway = $this->properties['gateway'];
			}
			
			if (file_exists(DIR_FILES_ELEMENTS . '/sixeightforms/payment/' . $gateway)) {
				Loader::element('sixeightforms/payment/' . $gateway . '/form',$vars);
			}
			
			if(file_exists(DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment/' . $gateway)) {
				Loader::packageElement('payment/' . $gateway . '/form','sixeightforms',$vars);
			}
		}
		
		public function loadGatewayProcessor($vars) {
			if($this->properties['gateway'] == '') {
				$gateway = 'paypal';
			} else {
				$gateway = $this->properties['gateway'];
			}
			
			if (file_exists(DIR_FILES_ELEMENTS . '/sixeightforms/payment/' . $gateway)) {
				Loader::element('sixeightforms/payment/' . $gateway . '/processor',$vars);
			}
			
			if(file_exists(DIR_BASE . '/' . DIRNAME_PACKAGES . '/sixeightforms/elements/payment/' . $gateway)) {
				Loader::packageElement('payment/' . $gateway . '/processor','sixeightforms',$vars);
			}
		}
	
	}
?>