.sem-field-container {
	padding: 5px 0;
}

.sem-label, .sem-legend {
	display:block;
	margin-top:4px;
}

.sem-form input, .sem-form textarea, .sem-form select {
	margin:2px;
}

.sem-fieldset {
	border:0;
	padding:0;
	margin:0;
}

.sem-legend {
	padding:0;
	margin:0;
	vertical-align:top;
	margin-top:4px;
	white-space:normal;
}

.sem-checkbox-label, .sem-radio-button-label {
	display:block;
	clear:both;
	margin-left:15px;
}