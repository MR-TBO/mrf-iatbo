﻿<?php 

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('functions.php'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Booking Form</title>
<link href="bookingform.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color:#F2F9FC;
}
.text3 {
	font-size: 12px;
}
-->
</style></head>

<body>
<div class="center" id="container" style="width:800px;">
<div id="InnerContent">

<img src="http://www.thebestof.co.uk/assets/images/logo/logo-best-of.png" alt="thebestof" />

<h1>Your email preferences</h1>
<ol>
<li>Enter your franchisee area (required).</li>
<li>Enter your name in the box labelled 'User 1'.</li>
<li>Enter any alias email addresses (no need to include the <em>@thebestof.co.uk</em> bit).</li>
<li>Repeat the process for any other members of your team.</li>
<li>Click the 'submit' button at the bottom of this form.</li>
</ol>
<div>

<form accept-charset="UTF-8" action="" method="POST" name="Form1" style="height:100%; margin:0" target="">

<?php
 

$isprocessed = ProcessForm();

if($isprocessed != true)
{
		
	if(isset($_POST['Submit']))	
	{
		echo '<h2>It looks like an error may have occurred - please try again</h2>';
	}
?>


		<label for="txtArea">Your area</label><br/> 
		<input type="text" name="txtArea" value="<?php echo getVal('txtArea'); ?>" /> <span class="red" >*</span>
		
		<div id="primaryuser">
		<div  class="user">
		<p>User 1</p>
		<input type="text" name="txtUser" value="<?php echo getVal('txtUser'); ?>" />
		</div>
		<div id="primaryuseremails" class="emails">
		<p>Alias Email Addresses</p>
		<?php for($ndx = 1;$ndx <= 5;$ndx++) { 
				$name = 'primaryuseremail[' . $ndx . ']';
		?>
			
		<p><input type="text" name="<?php echo $name ?>" value="<?php echo getVal('primaryuseremail',$ndx,true); ?>" /> @thebestof.co.uk</p>
			
		
		<?php } ?>	
		
		</div>
		<h3>This user is <strong>FREE</strong>.  Additional user accounts can be setup for an additional cost</h3> 
		</div>
		
		
		<div id="otherusers">
		<h4>Additional Users</h4>
		<?php for($userndx = 1;$userndx<=5;$userndx++) { 
		
			$otheruser = 'user[' . $userndx . ']';
			
			?>
		<div class="otheruser">
		
		<div  class="user">
		<p>User <?php echo $userndx + 1;?></p>
		<input type="text" name="<?php echo $otheruser;  ?>" value="<?php echo getVal('user',$userndx,true); ?>" /> @thebestof.co.uk
		</div>
		
		<div  class="emails">
		<p>Other email addresses</p>
		<?php for($ndx = 1;$ndx <= 5;$ndx++) { 
		
				$username = 'useremail[' . $userndx . '][' .  $ndx . ']';
			
		?>
			
			<p><input type="text" name="<?php echo $username ?>" value="<?php echo getVal2('useremail',$userndx,$ndx); ?>" /> @thebestof.co.uk</p> 
			
		
		<?php } ?>	
		</div>
		</div>
		<?php } ?>
		
		</div>
		
		<input value="Submit" name="Submit" class="button1" id="Submit" type="submit" />

<?php }
else
{
	
?>
	<h2>Thankyou - We have your details.</h2>
<?php 
}

?>		
		
</form>

</p>
</div>

<br clear="all" />
<div>
</div>

</div>
</div>
</body>
</html>
