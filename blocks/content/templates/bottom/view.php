<?php
	Loader::model('attribute/categories/collection');
	$c = Page::getCurrentPage();
	$containsPages = false;
	$cck = CollectionAttributeKey::getByHandle('local_area');
	$local_home = $c->getCollectionAttributeValue($cck);
	$local_home = strtolower($local_home);
	$local_home = str_replace(' ','-',$local_home);
	$local_home = str_replace('--','',$local_home);
?>
	<h2>Think you're the best?</h2>
	<p>So you've had a look around and want to sign up? No problem just fill in our online application form and we will get in touch.</p>
	<a href="/<?php echo $local_home; ?>/contact-us/">
		<img src="<?php echo $this->getThemePath(); ?>/images/global/contact-us.png" alt="Contact Us" class="contact-us"/>
	</a>