<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	Loader::model('attribute/categories/collection');
	Loader::model('attribute/categories/user');
	$aBlocks = $controller->generateNav();
	$c = Page::getCurrentPage();
	$containsPages = false;
	$cck = CollectionAttributeKey::getByHandle('local_area');
	$cckHT = CollectionAttributeKey::getByHandle('hover_text');
	$local_home = $c->getCollectionAttributeValue($cck);
	$local_home = strtolower($local_home);
	$local_home = str_replace('  ','',$local_home);
	$local_home = str_replace(' ','-',$local_home);
//	$local_home = $c->getCollectionAttributeValue(14);
	
	
	$nh = Loader::helper('navigation');
	
	//this will create an array of parent cIDs 
	$inspectC=$c;
	$selectedPathCIDs=array( $inspectC->getCollectionID() );
	$parentCIDnotZero=true;	
	while($parentCIDnotZero){
		$cParentID=$inspectC->cParentID;
		if(!intval($cParentID)){
			$parentCIDnotZero=false;
		}else{
			$selectedPathCIDs[]=$cParentID;
			$inspectC=Page::getById($cParentID);
		}
	}
	
	$adminLink = false;
	
	foreach($aBlocks as $ni) {
		$_c = $ni->getCollectionObject();
		if($_c->getCollectionHandle() == 'admin') {
			$adminLink = true;
		}
	}
	
	foreach($aBlocks as $ni) {
		$_c = $ni->getCollectionObject();
		if (!$_c->getCollectionAttributeValue('exclude_nav')) {
			
			
			$target = $ni->getTarget();
			if ($target != '') {
				$target = 'target="' . $target . '"';
			}
			if (!$containsPages) {
				// this is the first time we've entered the loop so we print out the UL tag
				$pageLink = $c->getCollectionHandle();
				if ($local_home == $pageLink) {
					echo '<ul class="nav'.($adminLink ? ' admin':'').'"><li class="home nav-path-selected"><a class="nav-path-selected" href="/'.$local_home.'/"><img src="'.$this->getThemePath().'/images/global/icon-home.png" /></a></li>';
				} else {
					echo '<ul class="nav'.($adminLink ? ' admin':'').'"><li class="home"><a href="/'.$local_home.'/"><img src="'.$this->getThemePath().'/images/global/icon-home.png" /></a></li>';
				}
			}
			
			$containsPages = true;
			
			$thisLevel = $ni->getLevel();
			if ($thisLevel > $lastLevel) {
				echo("<ul>");
			} else if ($thisLevel < $lastLevel) {
				for ($j = $thisLevel; $j < $lastLevel; $j++) {
					if ($lastLevel - $j > 1) {
						echo("</li></ul>");
					} else {
						echo("</li></ul></li>");
					}
				}
			} else if ($i > 0) {
				echo("</li>");
			}

			$pageLink = false;
			
			if ($_c->getCollectionAttributeValue('replace_link_with_first_in_nav')) {
				$subPage = $_c->getFirstChild();
				if ($subPage instanceof Page) {
					$pageLink = $nh->getLinkToCollection($subPage);
				}
			}
			
			if (!$pageLink) {
				$pageLink = $ni->getURL();
			}
			
			$u = new User();
			if ($u->checkLogin()) {
				$uID = $u->getUserID();
				$ui = UserInfo::getByID($uID);
				$redirectPage = $ui->getAttribute('login_attribute_redirect_url');
			}

			$gU = Group::getByName("iamthebestof"); 
			$gA = Group::getByName("Administrators"); 
			
			if($_c->getCollectionHandle() == 'admin') {
				if ($redirectPage == $pageLink || $u->inGroup($gU) || $u->inGroup($gA)) {
					if ($c->getCollectionID() == $_c->getCollectionID()) { 
						$hoverText = $_c->getCollectionAttributeValue($cckHT);
						echo('<li class="nav-selected nav-path-selected '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a class="nav-selected nav-path-selected" ' . $target . ' href="' . $pageLink . '" title="'.$hoverText.'">' . $ni->getName() . '</a>');
					} elseif ( in_array($_c->getCollectionID(),$selectedPathCIDs) ) { 
						$hoverText = $_c->getCollectionAttributeValue($cckHT);
						echo('<li class="nav-path-selected '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a class="nav-path-selected" title="'.$hoverText.'" href="' . $pageLink . '" ' . $target . '>' . $ni->getName() . '</a>');
					} else {
						$hoverText = $_c->getCollectionAttributeValue($cckHT);
						echo('<li class=" '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a href="' . $pageLink . '" ' . $target . ' title="'.$hoverText.'">' . $ni->getName() . '</a>');
					}
				}
			} else {
				if ($c->getCollectionID() == $_c->getCollectionID()) { 
					$hoverText = $_c->getCollectionAttributeValue($cckHT);
					echo('<li class="nav-selected nav-path-selected '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a class="nav-selected nav-path-selected" ' . $target . ' href="' . $pageLink . '" title="'.$hoverText.'">' . $ni->getName() . '</a>');
				} elseif ( in_array($_c->getCollectionID(),$selectedPathCIDs) ) { 
					$hoverText = $_c->getCollectionAttributeValue($cckHT);
					echo('<li class="nav-path-selected '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a class="nav-path-selected" title="'.$hoverText.'" href="' . $pageLink . '" ' . $target . '>' . $ni->getName() . '</a>');
				} else {
					$hoverText = $_c->getCollectionAttributeValue($cckHT);
					echo('<li class=" '.str_replace(' ', '-', str_replace('?', '', strtolower($ni->getName()))).'"><a href="' . $pageLink . '" ' . $target . ' title="'.$hoverText.'">' . $ni->getName() . '</a>');
				}	
			}

			
			
			
			$lastLevel = $thisLevel;
			$i++;
			
			
		}
	}
	
	$thisLevel = 0;
	if ($containsPages) {
		for ($i = $thisLevel; $i <= $lastLevel; $i++) {
			echo("</li></ul>");
		}
	}

?>
