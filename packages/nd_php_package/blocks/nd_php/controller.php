<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::block('library_file');

class NdPhpBlockController extends BlockController {
	
	var $pobj;

	protected $btTable = 'btNdPhpBlockContent';
	protected $btInterfaceWidth = "600";
	protected $btInterfaceHeight = "465";
	
	public $content = "";	
	
	public function getBlockTypeDescription() {
		return t("For adding PHP script by hand.");
	}
	
	public function getBlockTypeName() {
		return t("PHP");
	}	 
	
	public function __construct($obj = null) {		
		parent::__construct($obj); 
	}
	
	public function view(){ 
		$this->set('content', $this->content); 
	} 
	
	public function save($data) { 
		$args['content'] = isset($data['content']) ? $data['content'] : '';
		parent::save($args);
	}
}

?>