<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

$h = Loader::helper('concrete/interface'); 
$uh = Loader::helper('concrete/urls');
?>
<h1><span><?php  echo t('Import Data from CSV'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<b><?php  echo t('Note:'); ?></b> <?php  echo t('If using Excel for Mac, make sure you save your file as'); ?> "Windows Comma Separated".
	<form method="post" id="import-form" action="<?php  echo $this->url('/dashboard/sixeightforms/import', 'startImport')?>" enctype="multipart/form-data" >
		<table cellpadding="8" cellspacing="0" border="0">
			<tr>
				<td>
                	<?php  echo t('Select a File'); ?><br />
                    <input type="file" name="data_file" />
				</td>
				<td>
					<?php  echo t('Select a Form'); ?><br />
					<select name="fID">
						<option value="0">**New Form**</option>
						<?php  if(is_array($forms)) { foreach($forms as $form) { ?>
						<option value="<?php  echo $form->fID; ?>"><?php  echo $form->properties['name']; ?></option>
						<?php  } } ?>
					</select>
                <td>
                    <?php 
                    $b1 = $h->submit(t('Import'), 'import-form');
                    print $h->buttons($b1);
                    ?>
                </td>
			</tr>
		</table>
	</form>
</div>

<?php  if($csvNumRows > 0) { ?>
<h1><span>Importing Data</span></h1>
<div class="ccm-dashboard-inner">
	<h2><?php  echo $csvNumRows; ?> <?php  echo t('Columns Found'); ?></h2>
	<h3><?php  echo t('Map columns to form fields'); ?></h3>
	<form method="post" action="<?php  echo $this->url('/dashboard/sixeightforms/import', 'processImport')?>">
	<input type="hidden" name="formID" value="<?php  echo $f->fID; ?>" />
	<input type="hidden" name="fileID" value="<?php  echo $fileID; ?>" />
	<table cellpadding="8" cellspacing="0" border="0">
		<tr>
			<th><?php  echo t('CSV Column'); ?></th>
			<th><?php  echo t('Form Field'); ?></th>
			<th><?php  echo t('Convert new line to'); ?> &lt;br /&gt;?</th>
			<th><?php  echo t('Is file?'); ?></th>
		</tr>
		<?php  foreach($csvData as $key => $col) { ?>
		<tr>
			<td><?php  echo sixeightField::shortenText($col,25,''); ?></td>
			<td>
				<select name="col[<?php  echo $key; ?>]">
					<option value="0"><?php  echo t('Do not import'); ?></option>
					<option value="new"><?php  echo t('Create new field - ') . sixeightField::shortenText($col,25,''); ?></option>
					<option value="timestamp"><?php  echo t('Timestamp'); ?> (YYYY-MM-DD HH:MM:SS <?php  echo t('or'); ?> Unix Timestamp)</option>
					<option value="isApproved"><?php  echo t('Approval Status (true/false)'); ?></option>
					<option value="owner"><?php  echo t('Owner (Existent only)'); ?></option>
					<option value="createdOwner"><?php  echo t('Owner (Create if non-existent)'); ?></option>
					<?php  foreach($fields as $field) { ?>
						<option value="<?php  echo $field->ffID; ?>"><?php  echo $field->shortLabel; ?></option>
					<?php  } ?>
				</select>
			</td>
			<td align="center">
				<input type="checkbox" name="nl2br[<?php  echo $key; ?>]" value="1" />
			</td>
			<td align="center">
				<input type="checkbox" name="isFile[<?php  echo $key; ?>]" value="1" />
			</td>
		</tr>
		<?php  } ?>
		<tr>
			<td colspan="2">
			<input type="submit" value="Import" />
    		</td>
    	</tr>
	</table>
	</form>
</div>
<?php  } ?>

<?php  if($rowsImported > 0) { ?>
<h1><span><?php  echo t('Importing Complete'); ?></span></h1>
<div class="ccm-dashboard-inner">
<?php  echo $rowsImported; ?> <?php  echo t('rows imported.'); ?>
</div>
<?php  } ?>

<h1><span><?php  echo t('Form Converter'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<form method="get" id="convert-form" action="<?php  echo $this->url('/dashboard/sixeightforms/import', 'convertForm')?>">
	<?php  echo t('Convert any of your core form blocks to an advanced form.'); ?>
		<table cellpadding="8" cellspacing="0" border="0">
			<tr>
				<td>
					<?php  echo t('Select Form'); ?><br />
					<select name="bID">
					<?php  if(count($surveys) > 0) { ?>
						<?php  foreach($surveys as $survey) { ?>
						<option value="<?php  echo $survey['bID']; ?>"><?php  echo $survey['surveyName']; ?></option>
						<?php  } ?>
					<?php  } ?>
					</select>
				</td>
				<td>
					<?php 
		            $b1 = $h->submit(t('Convert'), 'convert-form');
		            print $h->buttons($b1);
		            ?>
				</td>
			</tr>
		</table>
	</form>
</div>

<h1><span><?php  echo t('Import/Export Form Definition'); ?></span></h1>
<div class="ccm-dashboard-inner">
	<h3><?php  echo t('You can export your form definition to XML to move it to a separate Concrete5 installation'); ?></h3>
	<form method="get" id="export-form" action="<?php  echo $this->url('/dashboard/sixeightforms/import', 'exportFormDefinition')?>">
		<table cellpadding="8" cellspacing="0" border="0">
			<tr>
				<td>
					<?php  echo t('Select Form'); ?><br />
					<select name="fID">
						<?php  if(is_array($forms)) { foreach($forms as $form) { ?>
						<option value="<?php  echo $form->fID; ?>"><?php  echo $form->properties['name']; ?></option>
						<?php  } } ?>
					</select>
				</td>
				<td>
					<?php 
		            $b1 = $h->submit(t('Export'), 'export-form');
		            print $h->buttons($b1);
		            ?>
				</td>
			</tr>
		</table>
		<pre>
<?php  echo htmlspecialchars($xml); ?>
		</pre>
	</form>
</div>