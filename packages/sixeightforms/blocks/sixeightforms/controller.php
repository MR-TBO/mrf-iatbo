<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('form','sixeightforms');
Loader::model('field','sixeightforms');
Loader::model('answer_set','sixeightforms');
Loader::model('form_style','sixeightforms');

class SixeightformsBlockController extends BlockController {

	protected $btTable = 'btSixeightforms';
	protected $btInterfaceWidth = "300";
	protected $btInterfaceHeight = "100";

	public function getBlockTypeDescription() {
		return t("Display a form created from the backend");
	}
	
	public function getBlockTypeName() {
		return t("Advanced Form");
	}
	
	function __construct($obj = null) {
		parent::__construct($obj);	
		$forms = sixeightForm::getAll();
		
		if(!count($forms)) {
			$this->set('isReady',false);
		} else {
			$this->set('isReady',true);
		}
	}
	
	function view() {
		global $c;
		$style = sixeightformstyle::getByID($this->sID);
		$this->set('style',$style);
		$form = sixeightForm::getByID($this->fID);
		$fields = $form->getFields();
		
		//If we are editing a record, we will get the editCode and answer set ID from the URL
		if(($_GET['editCode'] != '') && ($_GET['asID'] != '')) {
			//For security, this function only returns the answer set if the asID and the editCode match according to the DB
			$as = sixeightAnswerSet::getByIDAndEditCode($_GET['asID'],$_GET['editCode']);
			if(is_object($as)) {
				$this->set('editingRecord',true);
				//Populate the default values with the answers for each field
				foreach($fields as $key => $field) {
					$fields[$key]->defaultValue = $as->answers[$field->ffID]['value'];
				}
			}
		}
		
		$this->set('f',$form);
		$this->set('fID',$form->fID);
		$this->set('fields',$fields);
		$this->set('displayInDialog',$this->displayInDialog);
	}
	
	function on_page_view() {
		if($this->requireSSL == 1) {
			global $c;
			$cp = new Permissions($c);
			if (isset($cp)) {
				if (!$cp->canWrite() && !$cp->canAddSubContent() && !$cp->canAdminPage() && !$cp->canApproveCollection()) {	
					if($_SERVER['HTTPS']!="on") {
						$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
						header("Location:$redirect");
					}
				}
			}
		}
		
		$this->loadHeaderItems();
	}
	
	public function loadHeaderItems($fID = 0) {
		$uh = Loader::helper('concrete/urls');
		$html = Loader::helper('html');
		
		if($fID == 0) {
			$f = sixeightForm::getByID($this->fID);
		} else {
			$f = sixeightForm::getByID($fID);
		}
		
		$requiredFiles = array();
		
		$requiredFiles[] = $html->css($uh->getToolsURL('css?sID=' . $this->sID,'sixeightforms'),'sixeightforms');
		$requiredFiles[] = $html->javascript('ccm.ui.js'); //Required for ccm.sitemap.js, ccm.dialog.js, ccm.filemanager.js
		$requiredFiles[] = $html->javascript('ccm.dialog.js'); //Required to display any other sort of dialog (file manager, sitemap)
		$requiredFiles[] = $html->javascript('jquery.maxlength.js','sixeightforms');
		$requiredFiles[] = $html->css('ccm.dialog.css'); //Required to display the file manager (or any other sort of) dialog
		
		if($f->hasDateField()) {
			$requiredFiles[] = $html->javascript('jquery.ui.js'); //Required for date field 
			$requiredFiles[] = $html->css('jquery.ui.css'); //Required for date field
			$requiredFiles[] = $html->css('ccm.calendar.css');  //Required for date field
		}
		
		if($f->hasSiteMapField()) {
			$requiredFiles[] = $html->javascript('ccm.sitemap.js'); //Required to display the sitemap
		}
		
		if($f->hasFileManagerField() || $f->hasWYSIWYGField()) {
			$requiredFiles[] = $html->javascript('tiny_mce/tiny_mce.js'); //Load this because of some Javascript stuff that requires it in view.php
			$requiredFiles[] = $html->javascript('ccm.filemanager.js'); //Required for file manager
			$requiredFiles[] = $html->css('ccm.filemanager.css'); //Required for file manager
			$requiredFiles[] = $html->javascript('jquery.uploadify.js','sixeightforms');
			$requiredFiles[] = $html->javascript('swfobject.js');
			$requiredFiles[] = $html->css('ccm.search.css');
			$requiredFiles[] = $html->javascript('jquery.form.js');
			$requiredFiles[] = $html->javascript('ccm.search.js');
			$requiredFiles[] = $html->css('ccm.forms.css'); //Required for WYSIWYG Toolbar
			$requiredFiles[] = $html->css('ccm.menus.css');
			$requiredFiles[] = $html->javascript('jquery.liveupdate.js');
			$requiredFiles[] = '<script type="text/javascript" src="' . REL_DIR_FILES_TOOLS_REQUIRED . '/i18n_js"></script>'; //Required for file manager
		}

		foreach($requiredFiles as $rf) {
			$this->addHeaderItem($rf);
		}
	}

}

?>