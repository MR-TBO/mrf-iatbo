<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

$ch = Page::getByPath("/dashboard/sixeightforms/forms");
$chp = new Permissions($ch);
if (!$chp->canRead()) {
	die(_("Access Denied."));
}

Loader::library('view');
Loader::model('form','sixeightforms');
Loader::model('field','sixeightforms');
Loader::model('answer_set','sixeightforms');
$h = Loader::helper('concrete/interface');
$uh = Loader::helper('concrete/urls');

$form = sixeightForm::getByID(intval($_GET['fID']));
?>

<script type="text/javascript">
function createField() {
	$('#newFieldForm').submit();
}

$(document).ready(function() {

	//Select a field type
	$('.field-type-button').click(function() {
		var fieldType = $(this).attr('title');
		$('#type').val(fieldType);
		$('#field-type-title a').html($(this).html());
		$('#tab-type').fadeOut('fast', function() {
			$('#tab-options tr').hide();
			$('#field-row-title').show();
			$('#field-row-submit').show();
			switch(fieldType) {
				case 'Text (Single-line)' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Text (Multi-line)' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-height').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
				case 'Number' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Date' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-date-format').show();
					$('#field-row-expiration').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Time' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'File Upload' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-grouping').hide();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'File from File Manager' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-grouping').hide();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Email Address' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Phone Number' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-width').show();
					$('#field-row-max-input').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Dropdown' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-options').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Multi-Select' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-options').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-height').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Radio Button' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-options').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Checkbox' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-options').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'WYSIWYG' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-width').show();
					$('#field-row-height').show();
					$('#field-row-format').show();
					$('#field-row-toolbar').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Sellable Item' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-price').show();
					$('#field-row-start-quantity').show();
					$('#field-row-end-quantity').show();
					$('#field-row-increment').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Credit Card' :
					$('#field-row-required').show();
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-grouping').hide();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Hidden' :
					$('#field-row-label').show();
					$('#field-row-default-value').show();
					$('#field-row-ecommerce-name').show();
					$('#field-row-indexable').show();
					$('#field-row-url-parameter').show();
					$('#field-row-class').show();
					$('#field-row-container-class').show();
					break;
				case 'Text (no user input)' :
					$('#field-row-text').show();
					$('#field-row-container-class').show();
					break;
			}
			$('#tab-options').fadeIn('fast');
		});
	});
	
	//Change field type
	$('#field-type-title a').click(function(e) {
		e.preventDefault();
		$('#tab-options').fadeOut('fast', function() {
			$('#tab-type').fadeIn('fast');
		});
	});
	
<?php  
if(intval($_GET['ffID']) != 0) {
	$formsURL=View::url('/dashboard/sixeightforms/forms', 'updateField');
	$field = sixeightField::getByID(intval($_GET['ffID']));

	if(is_array($field->options)) {
		$optionList = '';
		foreach($field->options as $option) {
			$optionList .= $option['value'] . "\n";
		}
	}
?>
	$('.field-type-button').each(function() {
		if($(this).attr('title') == '<?php  echo $field->type; ?>') {
			$(this).click();	
		}
	});
<?php  
	foreach($field->getProperties() as $property => $value) {
		if(!is_array($value)) {
?>
			$('#<?php  echo $property; ?>').val('<?php  echo str_replace("\r\n",'',str_replace("'","\'",$value)); ?>');
<?php  
		}
	}
} else {
	$formsURL=View::url('/dashboard/sixeightforms/forms', 'addField');
}
?>

	$('.field-type-button').each(function() {
		if($(this).attr('id') == '<?php  echo $_GET['type']; ?>') {
			$(this).click();	
		}
	});
	
	//Submit form
	function createField() {
		$('#newFieldForm').submit();
	}
});
</script>
<style type="text/css">
.field-type-box {
	border-bottom:solid 1px #dedede;
	position:relative;
}

div.field-type-button {
	margin-top:2px;
	margin-bottom:2px;
	font-size:14px;
	padding:6px;
	border:solid 1px #fafafa;
	vertical-align:middle;
	color:#666666;
}

div.field-type-button:hover {
	background-color:#d9e7ff;
	border:solid 1px #94a7c7;
	cursor:pointer;
}

.field-type-title {
	height:18px;
}

.field-type-title {
	border:solid 1px #94a7c7;
	background-color:#d9e7ff;
}
</style>

<script type="text/javascript">
function populate_options(option_set) {
	switch(option_set) {
		case 'days':
			$('#numbers_form').hide();
			$('#options').val("<?php  echo t('Sunday'); ?>\n<?php  echo t('Monday'); ?>\n<?php  echo t('Tuesday'); ?>\n<?php  echo t('Wednesday'); ?>\n<?php  echo t('Thursday'); ?>\n<?php  echo t('Friday'); ?>\n<?php  echo t('Saturday'); ?>");
			break;
		case 'months':
			$('#numbers_form').hide();
			$('#options').val("<?php  echo t('January'); ?>\n<?php  echo t('February'); ?>\n<?php  echo t('March'); ?>\n<?php  echo t('April'); ?>\n<?php  echo t('May'); ?>\n<?php  echo t('June'); ?>\n<?php  echo t('July'); ?>\n<?php  echo t('August'); ?>\n<?php  echo t('September'); ?>\n<?php  echo t('October'); ?>\n<?php  echo t('November'); ?>\n<?php  echo t('December'); ?>");
			break;
		case 'states':
			$('#numbers_form').hide();
			$('#options').val("Alabama\nAlaska\nArizona\nArkansas\nCalifornia\nColorado\nConnecticut\nDelaware\nFlorida\nGeorgia\nHawaii\nIdaho\nIllinois\nIndiana\nIowa\nKansas\nKentucky\nLouisiana\nMaine\nMaryland\nMassachusetts\nMichigan\nMinnesota\nMississippi\nMissouri\nMontana\nNebraska\nNevada\nNew Hampshire\nNew Jersey\nNew Mexico\nNew York\nNorth Carolina\nNorth Dakota\nOhio\nOklahoma\nOregon\nPennsylvania\nRhode Island\nSouth Carolina\nSouth Dakota\nTennessee\nTexas\nUtah\nVermont\nVirginia\nWashington\nWashington, D.C.\nWest Virginia\nWisconsin\nWyoming\n");
			break;
		case 'countries':
			$('#numbers_form').hide();
			$('#options').val("Afghanistan\nAkrotiri\nAlbania\nAlgeria\nAmerican Samoa\nAndorra\nAngola\nAnguilla\nAntarctica\nAntigua and Barbuda\nArctic Ocean\nArgentina\nArmenia\nAruba\nAshmore and Cartier Islands\nAtlantic Ocean\nAustralia\nAustria\nAzerbaijan\nBahamas, The\nBahrain\nBaker Island\nBangladesh\nBarbados\nBelarus\nBelgium\nBelize\nBenin\nBermuda\nBhutan\nBolivia\nBosnia and Herzegovina\nBotswana\nBouvet Island\nBrazil\nBritish Indian Ocean Territory\nBritish Virgin Islands\nBrunei\nBulgaria\nBurkina Faso\nBurma\nBurundi\nCambodia\nCameroon\nCanada\nCape Verde\nCayman Islands\nCentral African Republic\nChad\nChile\nChina\nChristmas Island\nClipperton Island\nCocos (Keeling) Islands\nColombia\nComoros\nCongo, Democratic Republic of the\nCongo, Republic of the\nCook Islands\nCoral Sea Islands\nCosta Rica\nCote d'Ivoire\nCroatia\nCuba\nCyprus\nCzech Republic\nDenmark\nDhekelia\nDjibouti\nDominica\nDominican Republic\nEcuador\nEgypt\nEl Salvador\nEquatorial Guinea\nEritrea\nEstonia\nEthiopia\nFalkland Islands (Islas Malvinas)\nFaroe Islands\nFiji\nFinland\nFrance\nFrench Polynesia\nFrench Southern and Antarctic Lands\nGabon\nGambia, The\nGaza Strip\nGeorgia\nGermany\nGhana\nGibraltar\nGreece\nGreenland\nGrenada\nGuam\nGuatemala\nGuernsey\nGuinea\nGuinea-Bissau\nGuyana\nHaiti\nHeard Island and McDonald Islands\nHoly See (Vatican City)\nHonduras\nHong Kong\nHowland Island\nHungary\nIceland\nIndia\nIndian Ocean\nIndonesia\nIran\nIraq\nIreland\nIsle of Man\nIsrael\nItaly\nJamaica\nJan Mayen\nJapan\nJarvis Island\nJersey\nJohnston Atoll\nJordan\nKazakhstan\nKenya\nKingman Reef\nKiribati\nKorea, North\nKorea, South\nKuwait\nKyrgyzstan\nLaos\nLatvia\nLebanon\nLesotho\nLiberia\nLibya\nLiechtenstein\nLithuania\nLuxembourg\nMacau\nMacedonia\nMadagascar\nMalawi\nMalaysia\nMaldives\nMali\nMalta\nMarshall Islands\nMauritania\nMauritius\nMayotte\nMexico\nMicronesia, Federated States of\nMidway Islands\nMoldova\nMonaco\nMongolia\nMontenegro\nMontserrat\nMorocco\nMozambique\nNamibia\nNauru\nNavassa Island\nNepal\nNetherlands\nNetherlands Antilles\nNew Caledonia\nNew Zealand\nNicaragua\nNiger\nNigeria\nNiue\nNorfolk Island\nNorthern Mariana Islands\nNorway\nOman\nPacific Ocean\nPakistan\nPalau\nPalmyra Atoll\nPanama\nPapua New Guinea\nParacel Islands\nParaguay\nPeru\nPhilippines\nPitcairn Islands\nPoland\nPortugal\nPuerto Rico\nQatar\nRomania\nRussia\nRwanda\nSaint Barthelemy\nSaint Helena\nSaint Kitts and Nevis\nSaint Lucia\nSaint Martin\nSaint Pierre and Miquelon\nSaint Vincent and the Grenadines\nSamoa\nSan Marino\nSao Tome and Principe\nSaudi Arabia\nSenegal\nSerbia\nSeychelles\nSierra Leone\nSingapore\nSlovakia\nSlovenia\nSolomon Islands\nSomalia\nSouth Africa\nSouth Georgia and the South Sandwich Islands\nSouthern Ocean\nSpain\nSpratly Islands\nSri Lanka\nSudan\nSuriname\nSvalbard\nSwaziland\nSweden\nSwitzerland\nSyria\nTajikistan\nTanzania\nThailand\nTimor-Leste\nTogo\nTokelau\nTonga\nTrinidad and Tobago\nTunisia\nTurkey\nTurkmenistan\nTurks and Caicos Islands\nTuvalu\nUganda\nUkraine\nUnited Arab Emirates\nUnited Kingdom\nUnited States\nUnited States Pacific Island Wildlife Refuges\nUruguay\nUzbekistan\nVanuatu\nVenezuela\nVietnam\nVirgin Islands\nWake Island\nWallis and Futuna\nWest Bank\nWestern Sahara\nYemen\nZambia\nZimbabwe\nTaiwan\nEuropean Union");
			break;
		case 'numbers':
			$('#numbers_form').show();
			$('#autofill_start').val("");
			$('#autofill_end').val("");
			break;
		default:
			break;
	}
	$('#autofill_options').val("");
}

function fill_numbers() {
	options = "";
	 difference = $('#autofill_end').val() - $('#autofill_start').val();
	 number_of_options = Math.abs(difference);
	if (number_of_options > 100) {
		proceed = confirm('<?php  echo t('The values you have entered result in over 100 options.  This may take some time to fill, and depending on the exact range of numbers you have specified, it could potentially crash your browser.  Click OK to continue if you still want to continue.'); ?>');
	} else {
		proceed = 1;
	}
	
	if (proceed) {
		if (difference >=0) {
			for (i=$('#autofill_start').val();i<=$('#autofill_end').val();i++) {
				options += i + "\n";
			}
		} else {
			for (i=$('#autofill_start').val();i>=$('#autofill_end').val();i--) {
				options += i + "\n";
			}
		}
	}
	$('#options').val(options);
	$('#numbers_form').hide();
}
</script>

<form id="newFieldForm" action="<?php  echo $formsURL; ?>" method="POST">
<input type="hidden" name="fID" value="<?php  echo intval($_GET['fID']); ?>" />
<input type="hidden" name="ffID" value="<?php  echo intval($_GET['ffID']); ?>" />
<input type="hidden" name="type" id="type" />
	<table cellpadding="0" cellspacing="0" border="0" id="tab-type" class="ccm-tab" width="100%" <?php  if(($_GET['ffID'] != '') || ($_GET['type'] != '')) { echo 'style="display:none;"'; } ?> >
		<tr>
			<td>
				<h2><?php  echo t('Select a field type'); ?></h2>
				<hr />
				<div class="field-type-box">
					<div class="field-type-button" id="sem-text-single-line" title="Text (Single-line)"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/textfield.png" align="absmiddle" /> <?php  echo t('Text (Single-line)'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-text-multi-line" title="Text (Multi-line)"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/textarea.png" align="absmiddle"  /> <?php  echo t('Text (Multi-line)'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-number" title="Number"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/number.png" align="absmiddle"  /> <?php  echo t('Number'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-email-address" title="Email Address"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/email.png" align="absmiddle"  /> <?php  echo t('Email Address'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-dropdown" title="Dropdown"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/dropdown.png" align="absmiddle"  /> <?php  echo t('Dropdown'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-multi-select" title="Multi-Select"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/multi_select.png" align="absmiddle"  /> <?php  echo t('Multi-Select'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-radio-button" title="Radio Button"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/radio.png" align="absmiddle"  /> <?php  echo t('Radio Button'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-checkbox" title="Checkbox"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/checkbox.png" align="absmiddle"  /> <?php  echo t('Checkbox'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-date" title="Date"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/date.png" align="absmiddle"  /> <?php  echo t('Date'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-time" title="Time"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/time.png" align="absmiddle"  /> <?php  echo t('Time'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-file-upload" title="File Upload"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/file_upload.png" align="absmiddle"  /> <?php  echo t('File Upload'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-file-from-file-manager" title="File from File Manager"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/file_manager.png" align="absmiddle"  /> <?php  echo t('File from File Manager'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-wysiwyg" title="WYSIWYG"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/wysiwyg.png" align="absmiddle"  /> <?php  echo t('WYSIWYG'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-sellable-item" title="Sellable Item"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/sellable_item.png" align="absmiddle"  /> <?php  echo t('Sellable Item'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-credit-card" title="Credit Card"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/credit_card.png" align="absmiddle"  /> <?php  echo t('Credit Card'); ?></div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-hidden" title="Hidden"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/hidden.png" align="absmiddle" /> <?php  echo t('Hidden'); ?></div>
				</div>
				</div>
				<div class="field-type-box">
					<div class="field-type-button" id="sem-text-no-user-input" title="Text (no user input)"><img src="<?php  echo DIR_REL; ?>/packages/sixeightforms/images/text.png" align="absmiddle"  /> <?php  echo t('Text (no user input)'); ?></div>
				</div>
			</td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="0" border="0" id="tab-options" class="ccm-tab" width="100%" style="display:none">
		<tr class="field-row" id="field-row-title">
			<td colspan="2">
				<h2 id="field-type-title"><a href="#"><?php  echo t('Field Label'); ?></a></h2>
				<hr />
			</td>
		</tr>
		<tr class="field-row" id="field-row-required">
			<td><?php  echo t('Required'); ?></td>
			<td>
				<select name="required" id="required">
					<option value="">---</option>
					<option value="1"><?php  echo t('Yes'); ?></option>
					<option value="0"><?php  echo t('No'); ?></option>
				</select>
			</td>
		</tr>
		<tr class="field-row" id="field-row-label">
			<td><?php  echo t('Label'); ?></td>
			<td><input name="label" id="label" /></td>
		</tr>
		<tr class="field-row" id="field-row-text">
			<td colspan="2">
				<div><?php  echo t('Text'); ?></div>
				<textarea id="text" name="text" style="width:350px;height:180px;font-size:12px"></textarea>
			</td>
		</tr>
		<tr class="field-row" id="field-row-default-value">
			<td><?php  echo t('Default Value'); ?></td>
			<td><input name="defaultValue" id="defaultValue" /></td>
		</tr>
		<tr class="field-row" id="field-row-width">
			<td>
				<?php  echo t('Field Width'); ?>
				<div class="ccm-note"><?php  echo t('Pixels'); ?></div>
			</td>
			<td><input name="width" id="width" /></td>
		</tr>
		<tr class="field-row" id="field-row-height">
			<td>
				<?php  echo t('Field Height'); ?>
			</td>
			<td><input name="height" id="height" /></td>
		</tr>
		<tr class="field-row" id="field-row-max-input">
			<td>
				<?php  echo t('Maximum Input'); ?>
				<div class="ccm-note"><?php  echo t('Characters'); ?></div>
			</td>
			<td><input name="maxLength" id="maxLength" /></td>
		</tr>
		<tr class="field-row" id="field-row-format">
			<td><?php  echo t('Format'); ?></td>
			<td>
				<select name="format" id="format">
					<option value="basic"><?php  echo t('Basic'); ?></option>
					<option value="simple"><?php  echo t('Simple'); ?></option>
					<option value="advanced"><?php  echo t('Advanced'); ?></option>
					<option value="office"><?php  echo t('Office'); ?></option>
				</select>
			</td>
		</tr>
		<tr class="field-row" id="field-row-toolbar">
			<td><?php  echo t('Include C5 Toolbar?'); ?></td>
			<td>
				<select name="toolbar" id="toolbar">
					<option value="0">---</option>
					<option value="1"><?php  echo t('Yes'); ?></option>
					<option value="0"><?php  echo t('No'); ?></option>
				</select>
		</tr>
		<tr class="field-row" id="field-row-grouping">
			<td><?php  echo t('Group with Previous?'); ?></td>
			<td>
				<select name="groupWithPrevious" id="groupWithPrevious">
					<option value="">---</option>
					<option value="1"><?php  echo t('Yes'); ?></option>
					<option value="0"><?php  echo t('No'); ?></option>
				</select>
			</td>
		</tr>
		<tr class="field-row" id="field-row-price">
			<td><?php  echo t('Price Per Item'); ?></td>
			<td><input name="price" id="price" /></td>
		</tr>
		<tr class="field-row" id="field-row-start-quantity">
			<td><?php  echo t('Start Quantity'); ?></td>
			<td><input name="qtyStart" id="qtyStart" /></td>
		</tr>
		<tr class="field-row" id="field-row-end-quantity">
			<td><?php  echo t('End Quantity'); ?></td>
			<td><input name="qtyEnd" id="qtyEnd" /></td>
		</tr>
		<tr class="field-row" id="field-row-increment">
			<td><?php  echo t('Increment By'); ?></td>
			<td><input name="qtyIncrement" id="qtyIncrement" /></td>
		</tr>
		<tr class="field-row" id="field-row-options">
			<td><?php  echo t('Options'); ?></td>
			<td>
				<div class="ccm-note">
					<?php  echo t('Auto-populate:'); ?>
					<select id="auto-populate-option" onchange="populate_options(this.value);">
						<option value=""></option>
						<option value="days"><?php  echo t('Days of the Week'); ?></option>
						<option value="months"><?php  echo t('Months'); ?></option>
						<option value="states"><?php  echo t('U.S. States'); ?></option>
						<option value="countries"><?php  echo t('Countries'); ?></option>
						<option value="numbers"><?php  echo t('Numbers'); ?></option>
					</select>
					<div id="numbers_form" class="ccm-note" style="display:none"><?php  echo t('Start:'); ?><input id="autofill_start" size="4" /> <?php  echo t('End:'); ?><input style="font-size:10px" id="autofill_end" size="4"/> <a href="#" onclick="javascript:fill_numbers();"><?php  echo t('Fill'); ?></a></div>
				</div>
				<textarea id="options" name="options" value="options" rows="5" cols="40" style="font-size:11px"><?php  echo $optionList; ?></textarea>	
			</td>
		</tr>
		<tr id="field-row-date-format">
			<td>
				<?php  echo t('Date Format'); ?>
				<div class="ccm-note"><a href="http://docs.jquery.com/UI/Datepicker/formatDate" target="_blank"><?php  echo t('Formatting Options'); ?></a></div>
			</td>
			<td><input name="dateFormat" id="dateFormat" /></td>
		</tr>
		<tr id="field-row-expiration">
			<td>
				<?php  echo t('Use as expiration?'); ?>
			</td>
			<td>
				<select name="isExpirationField" id="isExpirationField">
					<option value="0"><?php  echo t('No'); ?></option>
					<option value="1"><?php  echo t('Yes'); ?></option>
				</select>
			</td>
		</tr>
        <?php  if($form->properties['gateway'] != '') { ?>
		<tr class="field-row" id="field-row-ecommerce-name">
			<td><?php  echo t('Send to payment gateway as'); ?></td>
			<td>
            	<?php  
                $form->loadGatewayConfig();
				$gateway = new semGateway();
				$gatewayFields = $gateway->getFields();
				?>
                <select name="eCommerceName" id="eCommerceName">
                <option value="">---</option>
                <?php  foreach ($gatewayFields as $key => $cf) { ?>
                	<option value="<?php  echo $key; ?>"><?php  echo $cf['label']; ?></option>
                <?php  } ?>
                </select>
			</td>
		</tr>
        <?php  } ?>
        <tr class="field-row" id="field-row-indexable">
        	<td>
        		<?php  echo t('Searchable?'); ?>
        		<div class="ccm-note"><?php  echo t('For Data Display integration'); ?></div>
        	</td>
        	<td valign="top">
        		<select name="indexable" id="indexable">
        			<option value="0"><?php  echo t('No'); ?></option>
        			<option value="1"><?php  echo t('Yes'); ?></option>
        		</select>
        	</td>
        </tr>
        <tr id="field-row-url-parameter">
			<td>
				<?php  echo t('URL Parameter'); ?>
				<div class="ccm-note"><?php  echo t('For specifying default value via URL'); ?></div>
			</td>
			<td valign="top"><input name="urlParameter" id="urlParameter" /></td>
		</tr>
		<tr id="field-row-class">
			<td>
				<?php  echo t('Field CSS Class'); ?>
			</td>
			<td valign="top"><input name="cssClass" id="cssClass" /></td>
		</tr>
		<tr id="field-row-container-class">
			<td>
				<?php  echo t('Container CSS Class'); ?>
			</td>
			<td valign="top"><input name="containerCssClass" id="containerCssClass" /></td>
		</tr>
		<tr id="field-row-submit">
			<td colspan="2" style="padding-top:15px;">
				<?php  echo $h->button_js( t('Save'), 'createField()','left'); ?>
			</td>
		</tr>
	</table>
</form>