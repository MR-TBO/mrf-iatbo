<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));

$ch = Page::getByPath("/dashboard/sixeightforms/forms");
$chp = new Permissions($ch);
if (!$chp->canRead()) {
	die(_("Access Denied."));
}

Loader::model('form','sixeightforms');
Loader::model('field','sixeightforms');
Loader::model('answer_set','sixeightforms');

$ih = Loader::helper('concrete/interface');
$uh = Loader::helper('concrete/urls');

$as = sixeightAnswerSet::getByID(intval($_GET['asID']));
?>
<table border="0" width="100%">
	<tr>
		<td width="50%">
			<h2><?php   echo t('Date Submitted'); ?></h2>
			<?php  echo date('F j, Y, g:i a',$as->dateSubmitted); ?>
		</td>
		<td width="50%">
			<h2><?php   echo t('IP Address'); ?></h2>
			<?php   echo $as->ipAddress; ?>
		</td>
	</tr>
    <?php   if ($as->amountCharged > 0) { ?>
    <tr>
    	<td>
        	<h2><?php  echo t('Amount Charged'); ?></h2>
            $<?php  echo $as->amountCharged; ?>
        </td>
        <td>
        	<h2><?php  echo t('Amount Paid'); ?></h2>
            $<?php  echo $as->amountPaid; ?>
        </td>
    </tr>
    <tr>
    	<td>
    		<h2><?php  echo t('Gateway Response'); ?></h2>
    		<?php  echo $as->gatewayResponse; ?>
    	</td>
    	<td>&nbsp;</td>
    </tr>
    <?php   } ?>
    <?php  if(Package::getByHandle('sixeightdatadisplay')) { ?>
    <tr>
    	<td colspan="2"><hr /></td>
    </tr>
    <tr>
    	<td>
    		<h2><?php  echo t('Associated Page'); ?></h2>
    		<div id="ascid-container">
    		<?php  
				$ps = Loader::helper('form/page_selector');
				print $ps->selectPage('asCID',$as->cID);
    		?>
    		</div>
    	</td>
    	<td>
    		<?php  echo $ih->button_js('Save Associated Page','saveAssociatedPage();','left'); ?>
<script type="text/javascript">
function saveAssociatedPage() {
	var asID = <?php  echo $as->asID; ?>;
	var cID = $('#ascid-container').find('[name=asCID]').val();
	$.ajax({
		type:'GET',
		url: '<?php  echo $uh->getToolsURL('save_associated_page','sixeightforms'); ?>?asID=' + asID + '&cID=' + cID,
		success: function(response) {
			alert('<?php  echo t('Page Saved'); ?>');
		}
	});
}
</script>
    	</td>
    </tr>
    <?php  } ?>
	<tr>
		<td colspan="2">
		<hr />
		<?php   
		foreach($as->answers as $a) { 
			$field = sixeightField::getByID($a['ffID']);
		?>
			<h2><?php  echo $field->label; ?></h2>
			<?php  
			if(($field->type == 'File from File Manager') || ($field->type == 'File Upload')) {
				$file=File::getByID($a['value']);
				if(($file) && (is_numeric($a['value']))) {
					$fv=$file->getApprovedVersion();
					echo $fv->getFileName();
				}
			} else {
				echo $a['value'];
			} 
			?>
			<br /><br />
		<?php   } ?>
		</td>
	</tr>
</table>