<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class ScrapbookSlideshowPackage extends Package {

	protected $pkgHandle = 'scrapbook_slideshow';
	protected $appVersionRequired = '5.3.2';
	protected $pkgVersion = '1.1.1';
	
	public function getPackageDescription() {
		return t("Create a slideshow based on a C5 Scrapbook.");
	}
	
	public function getPackageName() {
		return t("Scrapbook Slideshow");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('scrapbook_slideshow', $pkg);
	}




}