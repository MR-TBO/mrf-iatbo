<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
$scrapbookHelper=Loader::helper('concrete/scrapbook');
$sbId = 'scrapbookSlideshow' . intval($bID);
$sbContentClass = $sbId . '-content';

if($autoPlay == 1) {
	$delay = 0;
}

if($clickToProceed == 1) {
	$ctp = "next:'#$sbId',";
}

?>
<script type="text/javascript">
$(document).ready(function(){
	$('#<?php  echo $sbId; ?>').cycle({
		<?php  echo $ctp; ?>
		fx:'<?php  echo $effect; ?>',
		timeout: <?php  echo $delay; ?>,
		speed: <?php  echo $effectLength; ?>,
		sync:<?php  echo $syncEffects; ?>,
		pause:<?php  echo $pauseOnHover; ?>
	});
});
</script>
<?php  
	if(count($scrapbookBlocks)){
		echo '<div id="' . $sbId . '">';;
		//Loop through blocks to make tab content
		$i=1;
		foreach($scrapbookBlocks as $b) {
			$bv = new BlockView();
			echo '<div id="' . $sbId . '-' . $i . '" class="' . $sbContentClass . '">';
			echo  $bv->render($b, 'scrapbook');
			echo '</div>';
			$i++;
		}
		echo '</div>';
	}
?>
