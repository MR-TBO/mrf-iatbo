$('#addNewTab').click(function () {

	//Determine the current number of tabs
	var n = $('.tabTitle').size();
	var nextTab = n + 1;

	//Add the tab to the drop down
	$('#currentTab').append('<option value="' + nextTab + '">' + nextTab + '</option>');
	
	//Add the hidden form values
	$('#tabValues').append('<input type="hidden" class="tabTitle" name="title[' + nextTab + ']" /><input type="hidden" class="tabContent" id="content[' + nextTab + ']" />');
	
	//Select the new tab in the dropdown
	$('#currentTab').val(nextTab);
});

var TabbedContentBlock = {
	
	init:function(){},	
	
	editContent:function(){ 
		ccm_launchFileManager('&fType=' + ccmi18n_filemanager.FTYPE_IMAGE);
	}
	
}