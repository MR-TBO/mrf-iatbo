<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));

class JbYoutubeBlockController extends BlockController {

	protected $btTable = 'btJbYoutubetab';
	protected $btInterfaceWidth = "400";
	protected $btInterfaceHeight = "220";

	public $content = "";
	public $videowidth = 400;

	public function getBlockTypeDescription() {
		return "YouTube Pro Video Player";
	}

	public function getBlockTypeName() {
		return "YouTube Pro";
	}

	public function __construct($obj = null) {
		parent::__construct($obj);
	}


	public function view(){
		$this->set('content', $this->content);
		$this->set('videowidth', $this->videowidth);
	}

	public function save($data) {
		$args['videowidth'] = isset($data['videowidth']) ? $data['videowidth'] : 400;
		$args['content'] = isset($data['content']) ? $data['content'] : '';
		parent::save($args);
	}
		
}

?>