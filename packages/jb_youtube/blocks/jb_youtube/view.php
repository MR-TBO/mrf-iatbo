<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$url = parse_url($videoURL);
parse_str($url['query'], $query);
$c = Page::getCurrentPage();

$vWidth=425;
$vHeight=344;
if ($c->isEditMode()) { ?>
	<div class="ccm-edit-mode-disabled-item" style="width:<?php   echo $vWidth; ?>px; height:<?php   echo $vHeight; ?>px;">
		<div style="padding:8px 0px; padding-top: <?php   echo round($vHeight/2)-10; ?>px;"><?php   echo t('Content disabled in edit mode.'); ?></div>
	</div>
	
<?php   } else { ?>
<link rel="stylesheet" type="text/css" href="<?php  echo $this->getBlockURL();?>/js/youTubeEmbed-jquery-1.0.css" />
<div id="youtube<?php  echo $bID?>"></div>
<script src="<?php  echo $this->getBlockURL();?>/js/jquery.swfobject.1-1-1.min.js"></script>
<script src="<?php  echo $this->getBlockURL();?>/js/youTubeEmbed-jquery-1.0.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#youtube<?php  echo $bID?>').youTubeEmbed("<?php  echo $content;?>",<?php  echo $videowidth;?>,true);
});
</script>
<?php  }?>