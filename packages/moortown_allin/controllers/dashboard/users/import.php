<?php  

class DashboardUsersImportController extends Controller {

	public function upload() {
		if ($this->isPost()) {
		
			$u = new User();
			$uh = Loader::helper('concrete/user');
			$txt = Loader::helper('text');
			$vals = Loader::helper('validation/strings');
			$valt = Loader::helper('validation/token');
			$valc = Loader::helper('concrete/validation');
			$dtt = Loader::helper('form/date_time');
			$form = Loader::helper('form');
			$ih = Loader::helper('concrete/interface');
			$av = Loader::helper('concrete/avatar');
			$pkg = Package::getByHandle('moortown_allin');
			Loader::model('attribute/categories/user');

			
			$error = array();
			
			if (is_uploaded_file($_FILES['importFile']['tmp_name'])) {
				
				$file_handle = fopen($_FILES['importFile']['tmp_name'], "r");
				
				$i = 0;
				while (!feof($file_handle) ) {
					$line = fgetcsv($file_handle, 0);
					
					//check for required attributes
					if($i==0){//first row
						if($line[0]!='username'||$line[1]!='password'||$line[2]!='email'){
							$error[] = 'Header row not formatted correctly';
						}
						$importHandles = array();
						foreach ($line as $key => $value) {//loop through headers to check to see if handles are valid
							if($key > 2){
								
								$attribs = UserAttributeKey::getList();
								foreach($attribs as $ak) {
									$handleArray[] = $ak->getAttributeKeyHandle();
								}
								if(!in_array($value, $handleArray)){
									$error[] = 'Not a valid attribute: ' . strtoupper($value);
								} else {
									//$importHandles[] = $value;
								}
								$dataIndex = 'data_'+$key;
								$importHandles[$dataIndex] = $value;
							}
						}//end foreach
					}	
				$i++;
				}//end reading of the csv
				
				fclose($file_handle);


			} else {
				$error[] = "Select a CSV file to upload";			
			}

			
			if ($error) {
				$this->set('error', $error);
			} else { //DO THE IMPORT
				
				$file_handle = fopen($_FILES['importFile']['tmp_name'], "r");
				
				$i = 0;
				$importCount = 0;
				while (!feof($file_handle) ) {
					$line = fgetcsv($file_handle, 0);
					$import = 1;
					
					//check for required attributes
					if($i!=0){//first row, don't use for import
						
						$username = trim($line[0]);//clean the username
						$username = preg_replace("/\s+/", " ", $username);//clean the username
						
						$password = $line[1];
						
						$email = $line[2];
						
						//BEGIN VALIDATE USERNAME
						if (strlen($username) < USER_USERNAME_MINIMUM) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. Reason: %s',$uname,'Username too short');
							$import = 0;
						}
					
						if (strlen($username) > USER_USERNAME_MAXIMUM) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'Username too long');
							$import = 0;
						}
					
						if (strlen($username) >= USER_USERNAME_MINIMUM && !$valc->username($username)) {
							if(USER_USERNAME_ALLOW_SPACES) {
								$uname = strtoupper($username);
								$success_message[] = t('User %s not imported. REASON: %s',$uname,'A username may only contain letters, numbers and spaces');
								$import = 0;
							} else {
								$uname = strtoupper($username);
								$success_message[] = t('User %s not imported. REASON: %s',$uname,'A username may only contain letters or numbers.');
								$import = 0;
							}
						}
					
						if (!$valc->isUniqueUsername($username)) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'Username already exists');
							$import = 0;
						}		
					
						if ($username == USER_SUPER) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'Invalid Username');
							$import = 0;
						}
						//END VALIDATE USER NAME
						
						
						//BEGIN VALIDATE EMAIL
						if (!$vals->email($email)) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'Invalid email address provided.');
							$import = 0;
						} else if (!$valc->isUniqueEmail($email)) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'The email address is already in use.');
							$import = 0;
						}
						//END VALIDATE EMAIL
						
						//BEGIN VALIDATE PASSWORD
						if ((strlen($password) < USER_PASSWORD_MINIMUM) || (strlen($password) > USER_PASSWORD_MAXIMUM)) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'The password is not the right length');
							$import = 0;
						}
							
						if (strlen($password) >= USER_PASSWORD_MINIMUM && !$valc->password($password)) {
							$uname = strtoupper($username);
							$success_message[] = t('User %s not imported. REASON: %s',$uname,'A password may not contain ", \', >, <, or any spaces.');
							$import = 0;
						}
						//END VALIDATE PASSWORD
						
						//REGISTER THE USER
						if ($import!=0) {
							// do the registration
							$data = array('uName' => $username, 'uPassword' => $password, 'uEmail' => $email);
							$uo = UserInfo::add($data);
							
							$importCount++; 
							
							if(is_object($uo)){//enter attributes
								$in = 0;
								foreach ($line as $k => $v) {//loop through headers to check to see if handles are valid
									if($in > 2){
										//$success_message[] = $importHandles[$in];
										
										//if this is an address field, then we need to make this an array
										$uat = UserAttributeKey::getByHandle($importHandles[$in]);
										$uatype = $uat->getAttributeType();
										$typeName = $uatype->getAttributeTypeName();
										if($typeName=='Address'){
											//$success_message[] = 'Added Address';
											$addressArray = explode('|',$v);
											$addArray['address1'] = $addressArray[0];
											$addArray['address2'] = $addressArray[1];
											$addArray['city'] = $addressArray[2];
											$addArray['state_province'] = $addressArray[3];
											$addArray['country'] = $addressArray[4];
											$addArray['postal_code'] = $addressArray[5];
											$uo->setAttribute($importHandles[$in], $addArray);
										} else if($typeName=='Select'){
											$addArray = explode('|', $v);
											if(is_array($addArray)){
												$uo->setAttribute($importHandles[$in], $addArray);
											} else {
												$uo->setAttribute($importHandles[$in], $v);
											}
										} else { 
											$uo->setAttribute($importHandles[$in], $v);
										}
									}
								$in++;
								}//end foreach
							}//end if is object
							
							//enter groups
							if(is_array($_POST['group'])){
								foreach($_POST['group'] as $gk => $gv){
	
									$group = Group::getByID($gv);
									$uID = $uo->getUserID();
									$u = User::getByUserID($uID);
									$u->enterGroup($group);
								}
							}
							
						}
						//END REGISTER
					}	
					$i++;
				}//end reading of the csv
				
				fclose($file_handle);
				$success_message_final = $importCount . " user(s) imported.";
				
				if(is_array($success_message)){
					$success_message_final .= " Results: <ul><li>". implode("</li><li>", $success_message) . "</li></ul>";
				}
				
				$this->set('message', $success_message_final);
			}
			
		}
	}
	

	public function download() {
		
		Loader::model('attribute/categories/user');
	
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"AdvancedUserImport.csv\"");
		$headers	=	"username,password,email";
		
		$attribs = UserAttributeKey::getList();
		foreach($attribs as $ak) {
			$headers	.=	"," . $ak->getAttributeKeyHandle();
		}
		
		$headers	.=	"\n";
		echo $headers; 
		exit;
	}
			
		
}//END CLASS