<?php      

defined('C5_EXECUTE') or die(_("Access Denied."));

class MoortownAllinPackage extends Package {

	protected $pkgHandle = 'moortown_allin';
	protected $appVersionRequired = '5.4.1';
	protected $pkgVersion = '1.1'; 
	
	public function getPackageName() {
		return t("All In: The Advanced User Import Add-on"); 
	}	
	
	public function getPackageDescription() {
		return t("Allows for advanced import of users from a CSV file.");
	}
	 
	
	public function install() {
		$pkg = parent::install();		
		
		//install single pages for the dashboard
		Loader::model('single_page');
		$dau2 = SinglePage::add('/dashboard/users/import', $pkg);		
	}//end install

}//end class