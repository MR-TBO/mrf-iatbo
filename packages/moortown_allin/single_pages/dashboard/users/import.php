<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$form = Loader::helper('form');
$ast = Loader::helper('concrete/asset_library'); 
$ih = Loader::helper('concrete/interface');
?>

<h1><span><?php  echo t('Import')?></span></h1>
<div class="ccm-dashboard-inner">

<h2>Upload File</h2>
<form method="post" id="allin-import" enctype="multipart/form-data" action="<?php  echo $this->url('/dashboard/users/import/', 'upload')?>">

  <p>
    <label>CSV File<br/>
      <input type="file" name="importFile" id="importFile" />
    </label>
  </p>
 <?php  Loader::model('search/group');
	$gl = new GroupSearch();
	$gResults = $gl->getPage();
?> 

<h2>Add Users to Group(s)</h2>
<table id="groups_table" class="display">
  		<?php   if (is_array($gResults)) { ?>
		<tbody>
		<?php   foreach($gResults as $g): ?> 
			
			<tr>
				<td class="ccm-grid-cb groups"><input type="checkbox" class="" name="group[]" id="gID<?php  echo $g['gID']?>" value="<?php  echo $g['gID']?>" /></td>			
				<td><?php  echo $g['gName']?></td>
			</tr>
			
		<?php  endforeach; ?>
		</tbody>
		<?php   } ?>
</table>
          <div class="ccm-buttons">
          		<a href="javascript:void(0)" onclick="$('#allin-import').get(0).submit()" class="ccm-button"><span><?php  echo t('Import Users')?></span></a>
		</div>
		<div class="ccm-spacer">&nbsp;</div>	
	<p>&nbsp;</p>
	
<h2>Instructions</h2>
<ul>
<li>In your CSV file the first line should be the handles of the attributes you wish to import</li>
<li>The first attribute should be "username"</li>
<li>The second attribute should be "password"</li>
<li>The third attribute should be "email"</li>
<li>These are REQUIRED</li>
</ul>

<h2>Some Notes On Attribute Types</h2>
<ul>
<li>ADDRESS: The Concrete5 address attribute type requires some special formatting in your CSV
<ul>
<li>To fill out the full address field, separate your address by using the bar: |</li>
<li>An Example: Address Line 1|Address Line 2|City|State|Country|PostalCode</li>
<li>For the country, use the country's 2 digit code. Example: US or CA</li>
</ul>
</li>
<li>CHECKBOX
<ul><li>In your CSV you can enter "yes" for a selected box and leave your cell blank for an unselected one</li></ul>
</li>

<li>DATE
<ul>
<li>If you want just the date entered, format like this: 2/26/1976 (no need for leading zeros on the month or day)</li>
<li>If you want date and time entered, format like this: 2/26/1976 1:00:00 PM</li>
</ul>
</li>

<li>SELECT
<ul>
	<li>To fill multiple values separate them by a bar: |</li>
	<li>An Example: Value One|Value Two|Value Three</li>
</ul>
</li>

</ul>

<h2>Additional Notes</h2>
<ul>
<li>This import does NOT handle custom attributes (those outside of a default Concrete5 install), so don't try to import them.</li>
<li>This import detects whether a username or email is already in use. If they are, the import will ignore that user and continue importing. You will be notified of the offending user(s) -- and the reason for not importing -- when the import is finished.</li>
</ul>
<p><a href="<?php  echo $this->url('/dashboard/users/import/', 'download')?>">Download a Formatted CSV (Complete with all attributes)</a></p>
	

</div>