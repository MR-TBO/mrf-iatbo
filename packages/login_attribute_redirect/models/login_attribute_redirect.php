<?php      
defined('C5_EXECUTE') or die(_("Access Denied."));
class LoginAttributeRedirect {
	
	/**
	 * @return void
	 */
	public static function checkDoRedirect($controller) {
		
		$u = new User();
		if($u->isRegistered() && $u->isLoggedIn()) {
			
			// Redirect to rcID if set in all cases except the home page
			$rcID = $controller->post('rcID');
			if($rcID > 1) {
				$nh = Loader::helper('validation/numbers');
				//set redirect url
				if ($nh->integer($rcID)) {
					$rc = Page::getByID($rcID);
					$controller->redirect($rc->getCollectionPath()); 
					exit;
				}
			}
			$pkg = Package::getByHandle('login_attribute_redirect');
			Loader::model('user_attributes');
			
			
			// Redirect to next only attribute if it's set.
			$handle = $pkg->config('LOGIN_ATTRIBUTE_REDIRECT_NEXT_HANDLE');
			$ak = UserAttributeKey::getByHandle($handle);
			if($ak instanceof UserAttributeKey) {
				$ui = UserInfo::getByID($u->getUserID());
				$redir = $ui->getAttribute($ak);
				
				if(is_numeric($redir) && $redir > 0) { // was it a cID
					$page = Page::getByID($redir);
					if($page->getCollectionID() > 0) {
						$ui->setAttribute($handle,'');
						$controller->redirect($page->getCollectionPath());		
						exit;
					}
				}
				if(strlen($redir)) {
					$page = Page::getByPath($redir);
					if($page->getCollectionID() > 0) {
						$ui->setAttribute($handle,'');
						$controller->redirect($page->getCollectionPath());
						exit;
					}
				}
				
				if(strlen($redir) && $controller->isValidExternalUrl($redir)) {
					$ui->setAttribute($handle,'');
					$controller->externalRedirect( $redir );
					exit;
				}
			}
			
			// redirect to redirect attribute if it's set
			$handle = $pkg->config('LOGIN_ATTRIBUTE_REDIRECT_HANDLE');
			$ak = UserAttributeKey::getByHandle($handle);
			if($ak instanceof UserAttributeKey) {
				$ui = UserInfo::getByID($u->getUserID());
				$redir = $ui->getAttribute($ak);
				
				if(is_numeric($redir) && $redir > 0) { // was it a cID
					$page = Page::getByID($redir);
					if($page->getCollectionID() > 0) {
						$controller->redirect($page->getCollectionPath());		
						exit;
					}
				}
				if(strlen($redir)) {
					$page = Page::getByPath($redir);
					if($page->getCollectionID() > 0) {
						$controller->redirect($page->getCollectionPath());
						exit;
					}
				}
				
				if(strlen($redir) && $controller->isValidExternalUrl($redir)) {
					$controller->externalRedirect( $redir );
					exit;
				}
			}
		}
	}
	
}
?>